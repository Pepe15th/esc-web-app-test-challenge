'use strict';

const cors = require('cors');
const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware');
const noopServiceWorkerMiddleware = require('react-dev-utils/noopServiceWorkerMiddleware');
const path = require('path');
const paths = require('./paths');


module.exports = function(https, publicOrigin, disableHostCheck, allowedHosts) {
	// publicPath is left default ('/') as the assets are served at dev server root
	// noinspection WebpackConfigHighlighting
	return {
		features: ['compress', 'before', 'headers', 'middleware', 'after'],
		// WebpackDevServer 2.4.3 introduced a security fix that prevents remote
		// websites from potentially accessing local content through DNS rebinding:
		// https://github.com/webpack/webpack-dev-server/issues/887
		// https://medium.com/webpack/webpack-dev-server-middleware-security-issues-1489d950874a
		// However, it made several existing use cases such as development in cloud
		// environment or subdomains in development significantly more complicated:
		// https://github.com/facebookincubator/create-react-app/issues/2271
		// https://github.com/facebookincubator/create-react-app/issues/2233
		// So we let you override it if you really know what you're doing with
		// a special environment variable.
		disableHostCheck: disableHostCheck,
		// Enable gzip compression of generated files.
		compress: true,
		// Silence WebpackDevServer's own logs since they're generally not useful.
		// It will still show compile warnings and errors with this setting.
		clientLogLevel: 'none',
		// By default WebpackDevServer serves physical files from current directory
		// in addition to all the virtual build products that it serves from memory.
		// This is confusing because those files won’t automatically be available in
		// production build folder unless we copy them. However, copying the whole
		// project directory is dangerous because we may expose sensitive files.
		// Instead, put files in the public directory manually. In JavaScript code,
		// you can access URL of public folder with `process.env.PUBLIC_URL`.
		// Note that we only recommend to use public folder as an escape hatch
		// for files like `favicon.ico`, `manifest.json`, and libraries that are
		// for some reason broken when imported through Webpack. If you just want to
		// use an image, put it in source folder and `import` it from JavaScript
		// instead.
		contentBase: false,
		// Enable hot reloading server. It will provide /sockjs-node/ endpoint
		// for the WebpackDevServer client so it can learn when the files were
		// updated. The WebpackDevServer client is included as an entry point
		// in the Webpack development configuration. Note that only changes
		// to CSS are currently hot reloaded. JS changes will refresh the browser.
		hot: true,
		// WebpackDevServer is noisy by default so we emit custom message instead
		// by listening to the compiler events with `compiler.plugin` calls above.
		quiet: true,
		// Reportedly, this avoids CPU overload on some systems.
		// https://github.com/facebookincubator/create-react-app/issues/293
		// frontend/node_modules is not ignored to support absolute imports
		// https://github.com/facebookincubator/create-react-app/issues/1065
		watchOptions: {
			ignored: new RegExp(
				`^(?!${path
					.normalize(paths.appSrc + '/')
					.replace(/[\\]+/g, '\\\\')}).+[\\\\/]node_modules[\\\\/]`,
				'g'
			),
		},
		// Enable HTTPS if the HTTPS environment variable is set to 'true'
		https: https,
		overlay: false,
		allowedHosts: allowedHosts,
		before(app) {
			// This lets chunks and fonts be fetched from backend origin
			app.use(cors(publicOrigin));
			// This lets us open files from the runtime error overlay.
			app.use(errorOverlayMiddleware());
			// This service worker file is effectively a 'no-op' that will reset any
			// previous service worker registered for the same host:port combination.
			// We do this in development to avoid hitting the production cache if
			// it used the same host and port.
			// https://github.com/facebookincubator/create-react-app/issues/2272#issuecomment-302832432
			app.use(noopServiceWorkerMiddleware());
		},
	};
};
