<?php
declare(strict_types = 1);

namespace Netvor\Embryo\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


/**
 * Update 7d and 8d clusters classification
 */
class Version20180205191723 extends AbstractMigration
{
	public function up(Schema $schema)
	{
		$this->addSql('UPDATE `cluster` SET `classification` = \'7d\' WHERE `id` = 30');
		$this->addSql('UPDATE `cluster` SET `classification` = \'8d\' WHERE `id` = 31');
	}


	public function down(Schema $schema)
	{
		$this->addSql('UPDATE `cluster` SET `classification` = \'7da\' WHERE `id` = 30');
		$this->addSql('UPDATE `cluster` SET `classification` = \'8da\' WHERE `id` = 31');
	}
}
