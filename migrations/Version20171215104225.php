<?php
declare(strict_types=1);

namespace Netvor\Embryo\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


/**
 * Create table cluster, add cluster data
 */
class Version20171215104225 extends AbstractMigration
{
	public function up(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('CREATE TABLE cluster (id INT AUTO_INCREMENT NOT NULL, cati_id INT NOT NULL, classification VARCHAR(255) NOT NULL, development_phase VARCHAR(255) NOT NULL, short_name VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, implantation_chance SMALLINT NOT NULL, UNIQUE INDEX UNIQ_E5C5699437E6B75D (cati_id), UNIQUE INDEX UNIQ_E5C56994456BD231 (classification), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
		$this->addSql(file_get_contents(__DIR__ . '/clusters.sql'));
	}


	public function down(Schema $schema)
	{
		$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

		$this->addSql('DROP TABLE cluster');
	}
}
