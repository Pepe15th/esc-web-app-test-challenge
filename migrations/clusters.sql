INSERT INTO `cluster` (`id`, `cati_id`, `classification`, `development_phase`, `short_name`, `name`, `description`, `implantation_chance`) VALUES
	(1, 159, '1aa', 'HBlast', 'HBlast EarlExp', 'Hatching blastocyst with early onset of expansion', 'Hatching blastocyst starting expansion in 100 hrs after fertilization.
Expansion is linear without any collapse/re-expansions.
Correct cell cleavage up to 8cc stage: in 95%.
8cc stage reached in 55 hrs after fertilization.
The rate of fragmentation is less than 20%.', 70),
	(2, 212, '1ab', 'HBlast', 'HBlast MidExp', 'Hatching blastocyst with middle onset of expansion', 'Hatching blastocyst starting expansion after 100 hrs from fertilization.
Expansion is linear without any collapse/re-expansions.
Correct cell cleavage up to 8cc stage: in 90%.
8cc stage reached in 60 hrs after fertilization.
The rate of fragmentation is less than 20%.', 68),
	(3, 287, '1ba', 'HBlast', 'HBlast EarlCol', 'Hatching blastocyst with early collapse', 'Hatching blastocyst starting expansion in 100 hrs after fertilization.
Expansion is interrupted by one collapse of blastocoele occuring in 105 hrs after fetilization.
Correct cell cleavage up to 8cc stage: in 75%.
8cc stage reached in 60 hrs after fertilization..
The rate of fragmentation is less than 20%.', 60),
	(4, 293, '1bb', 'HBlast', 'HBlast LateCol', 'Hatching blastocyst with late collapse', 'Hatching blastocyst starting expansion after 100 hrs after fertilization.
Expansion is interrupted by one collapse of blastocoele occuring after 105 hrs after fetilization.
Correct cell cleavage up to 8cc stage: in 75%.
8cc stage reached in 60 hrs after fertilization.
The rate of fragmentation is less than 20%.', 50),
	(5, 288, '1bc', 'HBlast', 'HBlast MultCol', 'Hatching blastocyst with multiple collapses', 'Hatching blastocyst starting expansion in 110 hrs after fertilization.
Expansion is interrupted by multiple collapses of blastocoele.
Correct cell cleavage up to 8cc stage: in 70%.
8cc stage reached in 60 hrs after fertilization.
The rate of fragmentation is less than 20%.', 30),
	(6, 224, '1ca', 'HBlast', 'HBlast low cells', 'Hatching blastocyst with low number of trophectoderm cells', 'Hatching blastocyst with low number of trophectoderm cells.
This embryo has few TE cells, forming a loose epithelium and in extreme situation TE is formed by very few large cells.
Correct cell cleavage up to 8cc stage: in 50%.
Many of abnormally cleaved cells are excluded from the embryo into PVS (periviteline space) and are squeezed under ZP by growing blastocyst.
1 and more collapses of blastocoele can be detected.', 16),
	(7, 296, '1da', 'HBlast', 'HBlast deg', 'Hatching blastocyst with degeneration/vacuolization of majority of the cells', 'Hatching blastocyst with total collapse of blastocoel cavity without re-expansion.
The blastomeres undergo degeneration what is expressed by the total cell lysis or extreme vacuolization.
No further progress in development is achieved.
1 and more collapses of blastocoele can be detected before total collapse.', 2),
	(8, 185, '2aa', 'ExxBlast', 'ExxBlast EarlExp', 'Expanded blastocyst with early onset of expansion', 'Expanded blastocyst starting expansion in 100 hrs after fertilization.
Expansion is linear without any collapse/re-expansions.
Correct cell cleavage up to 8cc stage: in 95%.
8cc stage reached in 60 hrs after fertilization.
The rate of fragmentation is less than 20%.', 67),
	(9, 187, '2ab', 'ExxBlast', 'ExxBlast MidExp', 'Expanded blastocyst with middle onset of expansion', 'Expanded blastocyst starting expansion from 100-105 hrs after fertilization.
Expansion is linear without any collapse/re-expansions.
Correct cell cleavage up to 8cc stage: in 90%.
8cc stage reached in 65 hrs after fertilization.
The rate of fragmentation is less than 20%.', 65),
	(10, 158, '2ac', 'ExxBlast', 'ExxBlast LateExp', 'Expanded blastocyst with late onset of expansion', 'Expanded blastocyst starting expansion after 105 hrs after fertilization.
Expansion is linear without any collapse/re-expansions.
Correct cell cleavage up to 8cc stage: in 85%.
8cc stage reached in 70 hrs after fertilization.
The rate of fragmentation is less than 20%.', 62),
	(11, 179, '2ba', 'ExxBlast', 'ExxBlast EarlCol', 'Expanded blastocyst with early collapse', 'Expanded blastocyst starting expansion in 100 hrs after fertilization.
Expansion is interrupted by one collapse of blastocoele occurring 100-105 hrs after fertilization.', 55),
	(12, 181, '2bb', 'ExxBlast', 'ExxBlast LateCol', 'Expanded blastocyst with late collapse', 'Expanded blastocyst starting expansion in 105 hrs after fertilization.
Expansion is interrupted by one collapse of blastocoele occurring 105-110 hrs after fertilization.', 35),
	(13, 214, '2bc', 'ExxBlast', 'ExxBlast MultCol', 'Expanded blastocyst with multiple collapses', 'Expanded blastocyst starting expansion in 105 hrs after fertilization.
Expansion is interrupted by 2 and more collapses of blastocoele.', 25),
	(14, 225, '2ca', 'ExxBlast', 'ExxBlast low cells', 'Expanded blastocyst with low number of trophectoderm cells', 'Expanded blastocyst with low number of trophectoderm cells.
This embryo has few TE cells, forming a loose epithelium and in extreme situation TE is formed by very few large cells.
Many of abnormally cleaved cells are excluded from the embryo into PVS (peri viteline space) and are squeezed under ZP by growing blastocyst.
Expansion is interrupted by 1 and more collapses of blastocoele.', 15),
	(15, 301, '2da', 'ExxBlast', 'ExxBlast deg', 'Expanded blastocyst with degeneration/vacuolization of majority of the cells', 'Expanded blastocyst with total collapse of blastocoel cavity without re-expansion.
The blastomeres undergo degeneration what is expressed by the total cell lysis or extreme vacuolization.
No further progress in development is achieved.
1 and more collapses of blastocoele can be detected before total collapse.', 2),
	(16, 302, '3aa', 'ExBlast', 'ExBlast', 'Expanding blastocyst', 'Expanding blastocyst starting expansion in 105 hrs after fertilization.
The volume of the blastoceol cavity increased the diameter of the embryo and thinned zona pellucida.
Expansion is linear without any collapse/re-expansions.', 48),
	(17, 305, '3ba', 'ExBlast', 'ExBlast Col', 'Expanding blastocyst with one or more collapses', 'Expanding blastocyst starting expansion after 105 hrs after fertilization.
The volume of the blastoceol cavity increased the diameter of the embryo and thinned zona pellucida.
Expansion is interrupted by 1 and more collapses of blastocoele.', 18),
	(18, 308, '3ca', 'ExBlast', 'ExBlast low cells', 'Expanding blastocyst with low number of trophectoderm cells', 'Expanding blastocyst with low number of trophectoderm cells.
This embryo has few TE cells, forming a loose epithelium and in extreme situation TE is formed by very few large cells.
Many of abnormally cleaved cells are excluded from the embryo into PVS (peri viteline space) and are squeezed under ZP by growing blastocyst.
Expansion is interrupted by 1 and more collapses of blastocoele.', 10),
	(19, 309, '3da', 'ExBlast', 'ExBlast deg', 'Expanding blastocyst with degeneration/vacuolization of majority of the cells', 'Expanding blastocyst with total collapse of blastocoel cavity without re-expansion.
The blastomeres undergo degeneration what is expressed by the total cell lysis or extreme vacuolization.
No further progress in development is achieved.
1 and more collapses of blastocoele can be detected before total collapse.', 1),
	(20, 166, '4aa', 'Blast', 'Blast', 'Blastocyst', 'Blastocyst starting expansion after 105 hrs after fertilization.
Blastocoel cavity completely fills the embryo. No preiviteline space is visible.
The blastocyst volume still did not increase the diameter of the embryo to the extend to make zona pellucida thinner.
Expansion is linear without any collapse/re-expansions.', 17),
	(21, 190, '4ba', 'Blast', 'Blast Col', 'Blastocyst with one and more collapses', 'Blastocyst with 1 and more collapses of blastocoele.
Blastocyst starting expansion after 105 hrs after fertilization.
Blastocoel cavity completely fills the embryo.
No preiviteline space is visible.
The blastocyst volume still did not increase the diameter of the embryo to the extend to make zona pellucida thinner.
Expansion is interrupted by 1 and more collapses of blastocoele.', 12),
	(22, 290, '4ca', 'Blast', 'Blast low cells', 'Blastocyst with low number of trophectoderm cells', 'Blastocyst with low number of trophectoderm cells.
TE is formed by very few large cells.
Many of abnormally cleaved cells are excluded from the embryo into PVS (peri viteline space) and are squeezed under ZP by growing blastocyst.
The high rate of fragmentation is observed (more than 50%).
1 and more collapses of blastocoele can be detected.', 7),
	(23, 291, '4da', 'Blast', 'Blast deg', 'Blastocyst with degeneration/vacuolization of majority of the cells', 'Blastocyst with total collapse of blastocoel cavity without re-expansion.
The blastomeres undergo degeneration what is expressed by the total cell lysis or extreme vacuolization.
No further progress in development is achieved.
1 and more collapses of blastocoele can be detected before total collapse.', 1),
	(24, 197, '5aa', 'EBlast', 'EBlast', 'Early blastocyst', 'Early blastocyst with visible blastocoel cavity formation.
The cavity fills less than half volume of the embryo.
Perivitelline space (PVS) is still present and no degenerated cells are found in PVS.
The rate of fragmentation is less than 50%.', 9),
	(25, 310, '5ca', 'EBlast', 'EBlast low cells', 'Early blastocyst with low number of cells', 'Early blastocyst with visible blastocoel cavity having low number of cells.
Many of abnormally cleaved cells are excluded from the embryo into PVS (periviteline space).
The high rate of fragmentation is observed (more than 50 %).
Expansion is interrupted by 1 and more collapses of blastocoele.', 4),
	(26, 311, '5da', 'EBlast', 'EBlast deg', 'Early blastocyst with degeneration/vacuolization of majority of the cells', 'Early blastocyst with total collapse of blastocoel cavity without re-expansion.
The blastomeres undergo degeneration what is expressed by the total cell lysis or extreme vacuolization.
No further progress in development is achieved.
1 and more collapses of blastocoele can be detected before total collapse.', 1),
	(27, 170, '6aa', 'Morula', 'Morula', 'Morula', 'Morula stage with compacted blastomeres (16 and more cells).
No blastocoel cavity is visible.
No degenerated cells are found in PVS (perivitelline space).
The rate of fragmentation is less than 50%.', 3),
	(28, 312, '6ca', 'Morula', 'Morula low cells', 'Morula with low number of cells', 'Morula stage with compacted blastomeres.
No cavity is visible.
Embryo is formed by very few large cells (up to 8).
Some lysed and/or vaculolized blastomeres are excluded from the embryo into PVS (peri viteline space).
The high rate of fragmentation is observed (more than 50%).', 1),
	(29, 313, '6da', 'Morula', 'Morula deg', 'Morula with degeneration/vacuolization of majority of the cells', 'Morula stage with degeneration/vacuolization of majority of the cells.
The blastomeres undergo degeneration.
The degeneration is expressed by the total cell lysis or extreme vacuolization.
No further progress in development is achieved.', 0),
	(30, 209, '7da', 'Arrested', 'Arrested', 'Arrested/degenerated', 'Arrested/degenerated embryos.
The embryos are arrested in their development. There is no progress in cells\' cleavages.
The cells frequently undergo degeneration and vacuolization/lysis.
The development can be stopped at any time of embryo development (from 2cc stage to more than 8-16cc).
No signs of the cell compaction are visible.', 0),
	(31, 169, '7dd', 'Unfertilized', 'Unfertilized', 'Unfertilized', 'Unfertilized oocytes - no cleavages.
No signs of pronuclei formation. In some cases PN are present but no cleavage occurs.
The oocytes can be lysed soon after ICSI or can undergo degeneration during prolonged culture.
In the case of extreme vacuole formation the oocyte can increase its diameter what is followed by the total lysis of the cell.', 0);
