<?php
declare(strict_types=1);

namespace Netvor\Embryo\ApiModule\Presenters;

use Nette;
use Nette\Utils\Arrays;
use Netvor;
use Netvor\Embryo\ApiModule\Model\EmbryoService;
use Netvor\Embryo\Model\Entities\Cluster;
use Netvor\Embryo\Model\UserService;


class LearningPresenter extends BaseLoggedInPresenter
{
	private const CACHE_EXPIRATION = '1 day';

	/** @var EmbryoService */
	private $model;

	/** @var UserService */
	private $userModel;

	/** @var Nette\Caching\Cache */
	private $cache;


	public function __construct(EmbryoService $model, UserService $userModel, Nette\Caching\Cache $cache)
	{
		$this->model = $model;
		$this->userModel = $userModel;
		$this->cache = $cache;
	}


	public function getAll(Nette\Application\Request $request): Nette\Application\IResponse
	{
		$user = $this->getUserIdentity();

		$key = [__CLASS__, __METHOD__];
		if (($cached = $this->cache->load($key)) !== null) {
			return $this->json(array_merge([
				'introductionVisited' => $user->isIntroductionVisited(),
				'stagesOfDevelopmentVisited' => $user->isStagesOfDevelopmentVisited(),
				'embryoQualityVisited' => $user->isEmbryoQualityVisited(),
				'eventsVisited' => $user->isEventsVisited(),
				'implantationChanceVisited' => $user->isImplantationChanceVisited(),
			], $cached));
		}

		$clusterCategories = $this->model->getClusterCategories();
		$learningDescriptions = $this->model->getLearningDescriptions();

		$clusters = $this->model->getClusters();

		/** @var array $clusterStructure */
		$clusterStructure = Arrays::associate(Arrays::map($clusters, function (Cluster $cluster) {
			$classification = $cluster->getClassification();
			return [
				'stage' => substr($classification, 0, 1),
				'quality' => substr($classification, 0, 2),
				'classification' => $classification,
				'cluster' => $cluster,
			];
		}), 'stage|quality|classification=cluster');

		$categoriesByQuality = Arrays::flatten(Arrays::map($clusterCategories, function (array $category) {
			return array_fill_keys($category['clustersByQuality'], $category['classification']);
		}), true);
		$colors = $this->model->getClusterColors();

		$embryos = $this->model->getRepresentativeEmbryos();
		$annotations = $this->model->getAnnotations(array_column($embryos, 'EmbryoId'));

		$clustersByStage = Arrays::map($clusterStructure, function (array $byQuality, $stage) use (
			$categoriesByQuality,
			$learningDescriptions,
			$colors,
			$embryos,
			$annotations
		) {
			/** @var Cluster $firstCluster */
			$firstCluster = array_values(array_values($byQuality)[0])[0];
			$embryo = $embryos[$firstCluster->getClassification()] ?? null;
			$stageName = Nette\Utils\Strings::split($firstCluster->getName(), '~ with |/~')[0];
			$qualityDescriptions = $learningDescriptions[$stage]['byQuality'] ?? [];
			$phase = $firstCluster->getDevelopmentPhase();
			return [
				'classification' => (string) $stage,
				'shortName' => $firstCluster->getDevelopmentPhase(),
				'name' => $stageName,
				'description' => $learningDescriptions[$stage]['description'] ?? $firstCluster->getDescription(),
				'embryo' => $embryo !== null ? $this->model->formatEmbryo($embryo, $annotations, $firstCluster, $phase) : null,
				'clustersByQuality' => Arrays::map($byQuality, function (array $byEvents, $quality) use (
					$categoriesByQuality,
					$colors,
					$embryos,
					$annotations,
					$qualityDescriptions
				) {
					/** @var Cluster $firstCluster */
					$firstCluster = array_values($byEvents)[0];
					$embryo = $embryos[$firstCluster->getClassification()] ?? null;
					$eventDescriptions = $qualityDescriptions[$quality]['byEvents'] ?? [];
					$phase = $firstCluster->getDevelopmentPhase();
					return [
						'classification' => $quality,
						'category' => $categoriesByQuality[$quality] ?? null,
						'shortName' => $qualityDescriptions[$quality]['shortName'] ?? $firstCluster->getShortName(),
						'name' => $qualityDescriptions[$quality]['name'] ?? $firstCluster->getName(),
						'description' => $qualityDescriptions[$quality]['description'] ?? $firstCluster->getDescription(),
						'embryo' => $embryo !== null ? $this->model->formatEmbryo($embryo, $annotations, $firstCluster, $phase) : null,
						'clustersByEvents' => Arrays::map($byEvents, function (Cluster $cluster, $event) use (
							$colors,
							$embryos,
							$annotations,
							$phase,
							$eventDescriptions
						) {
							$embryo = $embryos[$cluster->getClassification()] ?? null;

							return [
								'classification' => $cluster->getClassification(),
								'shortName' => $cluster->getShortName(),
								'name' => $eventDescriptions[$event]['name'] ?? $cluster->getName(),
								'description' => $eventDescriptions[$event]['description'] ?? $cluster->getDescription(),
								'implantationChance' => $cluster->getImplantationChance(),
								'color' => $colors[$cluster->getClassification()] ?? null,
								'embryo' => $embryo !== null ? $this->model->formatEmbryo($embryo, $annotations, $cluster, $phase) : null,
							];
						}),
					];
				}),
			];
		});

		return $this->json(array_merge([
			'introductionVisited' => $user->isIntroductionVisited(),
			'stagesOfDevelopmentVisited' => $user->isStagesOfDevelopmentVisited(),
			'embryoQualityVisited' => $user->isEmbryoQualityVisited(),
			'eventsVisited' => $user->isEventsVisited(),
			'implantationChanceVisited' => $user->isImplantationChanceVisited(),
		], $this->cache->save($key, [
			'clusterCategories' => $clusterCategories,
			'clustersByStage' => $clustersByStage,
		], [
			Nette\Caching\Cache::EXPIRE => self::CACHE_EXPIRATION,
		])));
	}


	public function post(array $data, Nette\Application\Request $request): Nette\Application\IResponse
	{
		$sections = Netvor\Embryo\Model\Entities\User::LEARNING_SECTIONS;
		/** @var string $section */
		$section = Netvor\Embryo\Utils\Validator::validateField($data, 'section', [$sections], $errors);
		if (!empty($errors)) {
			throw $this->error(implode("\n", $errors), 400);
		}

		$this->userModel->visitLearning($this->getUserIdentity(), $section);
		return $this->json(['success' => true]);
	}
}
