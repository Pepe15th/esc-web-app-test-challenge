<?php
declare(strict_types=1);

namespace Netvor\Embryo\ApiModule\Presenters;

use Nette;
use Nette\Utils\Arrays;
use Netvor\Embryo\ApiModule\Model\EmbryoService;
use Netvor\Embryo\Model\Entities\Cluster;


class CsvPresenter extends BaseLoggedInPresenter
{
	private const CACHE_EXPIRATION = '1 day';

	/** @var int */
	public $limit = 5;

	/** @var EmbryoService */
	private $model;

	/** @var Nette\Caching\Cache */
	private $cache;

	/** @var Nette\Security\User */
	private $user;

	/** @var Nette\Application\LinkGenerator */
	private $linkGenerator;


	public function __construct(
		EmbryoService $model,
		Nette\Caching\Cache $cache,
		Nette\Security\User $user,
		Nette\Application\LinkGenerator $linkGenerator
	) {
		$this->model = $model;
		$this->cache = $cache;
		$this->user = $user;
		$this->linkGenerator = $linkGenerator;
	}


	public function run(Nette\Application\Request $request): Nette\Application\IResponse
	{
		if (!$this->user->isLoggedIn()) {
			$url = $this->linkGenerator->link('Login:');
			return new Nette\Application\Responses\RedirectResponse($url);
		}
		return parent::run($request);
	}


	public function getAll(Nette\Application\Request $request): Nette\Application\IResponse
	{
		$key = [__CLASS__, __METHOD__, $this->limit];
		if (($cached = $this->cache->load($key)) !== null) {
			return $this->createResponse(...$cached);
		}

		$clusters = Arrays::associate(Arrays::map($this->model->getClusters(), function (Cluster $cluster) {
			return [
				'catiId' => $cluster->getCatiId(),
				'cluster' => $cluster,
			];
		}), 'catiId=cluster');

		$embryos = $this->model->getEmbryosByClusters(array_keys($clusters), $this->limit);
		$events = Arrays::map(
			$this->model->getAnnotations(array_column($embryos, 'EmbryoId')),
			function (array $annotations) {
				return $this->model->formatEvents($annotations, -1, '', []);
			}
		);

		$columns = [];
		$divisions = [];
		foreach ($events as $id => $embryoEvents) {
			$divisions[$id] = [];
			foreach ($embryoEvents as $frame => $frameEvents) {
				foreach ($frameEvents as $division) {
					if ($division['type'] !== 'division') {
						continue;
					}
					$count = $division['cell_count'];
					$columns[$count] = '';
					$divisions[$id][$count] = $frame;
				}
			}
			ksort($divisions[$id]);
		}
		ksort($columns);

		$headers = array_merge(['EmbryoId', 'Cluster'], Arrays::map(array_keys($columns), function (int $column) {
			return sprintf('%d cells', $column);
		}));

		$data = Arrays::map($embryos, function (array $embryo) use ($clusters, $columns, $divisions) {
			$id = $embryo['EmbryoId'];
			$embryoDivisions = Arrays::map($divisions[$id], function (int $frame) use ($embryo) {
				$minutes = $frame * EmbryoService::MINUTES_PER_FRAME + $embryo['OffsetMinutes'];
				return sprintf('%d:%02d', $minutes / 60, $minutes % 60);
			});
			return array_replace([
				'EmbryoId' => $id,
				'Cluster' => $clusters[$embryo['CurveClass']]->getClassification(),
			], $columns, $embryoDivisions);
		});

		usort($data, function ($a, $b) {
			return [$a['Cluster'], $a['EmbryoId']] <=> [$b['Cluster'], $b['EmbryoId']];
		});

		return $this->createResponse(...$this->cache->save($key, [$headers, $data], [
			Nette\Caching\Cache::EXPIRE => self::CACHE_EXPIRATION,
		]));
	}


	private function createResponse(array $headers, array $data): Nette\Application\IResponse
	{
		$callback = function (Nette\Http\IRequest $_, Nette\Http\IResponse $httpResponse) use ($headers, $data) {
			$httpResponse->setContentType('text/csv', 'utf-8');
			$disposition = sprintf(
				'attachment; filename="%s"; filename*=utf-8\'\'%s',
				'embryos.csv',
				rawurlencode('embryos.csv')
			);
			$httpResponse->setHeader('Content-Disposition', $disposition);

			$handle = fopen('php://output', 'w');
			fputcsv($handle, $headers);
			foreach ($data as $embryoData) {
				fputcsv($handle, array_values($embryoData));
			}
		};

		return new Nette\Application\Responses\CallbackResponse($callback);
	}
}
