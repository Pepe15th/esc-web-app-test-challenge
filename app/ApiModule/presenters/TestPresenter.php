<?php
declare(strict_types=1);

namespace Netvor\Embryo\ApiModule\Presenters;

use Nette;
use Nette\Application;
use Nette\Utils\Arrays;
use Netvor;
use Netvor\Embryo\ApiModule\Model\EmbryoService;
use Netvor\Embryo\Model\Entities\Cluster;
use Netvor\Embryo\Model\Entities\TestQuestion;
use Netvor\Embryo\Model\Entities\User;
use Netvor\Embryo\Model\TestService;
use Netvor\Embryo\Model\TrainingService;
use Tracy\ILogger;


class TestPresenter extends BaseLoggedInPresenter
{

	/** @var TestService */
	private $model;

	/** @var TrainingService */
	private $trainingModel;

	/** @var EmbryoService */
	private $embryoModel;

	/** @var ILogger */
	private $logger;


	public function __construct(TestService $model, TrainingService $trainingModel, EmbryoService $embryoModel, ILogger $logger)
	{
		$this->model = $model;
		$this->trainingModel = $trainingModel;
		$this->embryoModel = $embryoModel;
		$this->logger = $logger;
	}


	public function post(array $data, Application\Request $request): Application\IResponse
	{
		$user = $this->getUserIdentity();
		$trainingCount = $this->trainingModel->getCurrentCount($user);
		$accuracy = $this->trainingModel->getAccuracy($user);

		if ($trainingCount < TrainingService::MIN_QUESTION_COUNT || $accuracy < TrainingService::MIN_ACCURACY) {
			throw $this->error('You have to complete training first', 403);
		}

		$clusters = $this->embryoModel->getClusters();
		$clustersByCatiId = Arrays::associate(Arrays::map($clusters, function (Cluster $cluster) {
			return [
				'catiId' => $cluster->getCatiId(),
				'cluster' => $cluster,
			];
		}), 'catiId=cluster');

		$test = $this->model->fetchOrCreateTest($clusters, $user);
		if ($test === null) {
			throw $this->error('You have no tests left.', 400);
		}

		$embryoIds = $test->getQuestions()->map(function (TestQuestion $question) {
			return $question->getEmbryoId();
		})->getValues();
		$embryos = $this->embryoModel->getEmbryosByIds($embryoIds);
		$annotations = $this->embryoModel->getAnnotations(array_column($embryos, 'EmbryoId'));
		$descriptions = $this->embryoModel->getLearningDescriptions();

		if (count($embryos) !== $test->getQuestions()->count()) {
			$missingEmbryos = array_diff($embryoIds, array_keys($embryos));
			$message = sprintf('Embryos %s not found.', implode(', ', Arrays::map($missingEmbryos, function ($id) {
				return sprintf('#%d', $id);
			})));
			$this->logger->log(new \RuntimeException($message), ILogger::EXCEPTION);
			throw $this->error($message, 404);
		}

		$missingClusters = array_diff(array_column($embryos, 'CurveClass'), array_keys($clustersByCatiId));
		if (count($missingClusters) > 0) {
			$message = sprintf('Clusters %s not found.', implode(', ', Arrays::map($missingClusters, function ($id) {
				return sprintf('#%d', $id);
			})));
			$this->logger->log(new \RuntimeException($message), ILogger::EXCEPTION);
			throw $this->error($message, 404);
		}

		return $this->json([
			'testId' => $test->getId(),
			'level' => $level = $test->getLevel(),
			'questions' => $test->getQuestions()->map(function (TestQuestion $question) use (
				$clusters,
				$clustersByCatiId,
				$embryos,
				$annotations,
				$level,
				$descriptions
			) {
				$embryo = $embryos[$question->getEmbryoId()];
				$cluster = $clustersByCatiId[$embryo['CurveClass']];
				$options = TestQuestion::getOptionsByCluster($clusters, $cluster, $level);
				return [
					'questionId' => $question->getId(),
					'embryo' => $this->embryoModel->formatEmbryo($embryo, $annotations),
					'options' => $this->formatOptions($options, $level, $descriptions),
				];
			})->getValues(),
		]);
	}


	public function put(string $id, array $data, Application\Request $request): Application\IResponse
	{
		$user = $this->getUserIdentity();
		$trainingCount = $this->trainingModel->getCurrentCount($user);
		$accuracy = $this->trainingModel->getAccuracy($user);

		if ($trainingCount < TrainingService::MIN_QUESTION_COUNT || $accuracy < TrainingService::MIN_ACCURACY) {
			throw $this->error('You have to complete training first', 403);
		}

		try {
			Nette\Utils\Validators::assert($id, 'numericint:1..', 'id');
		} catch (Nette\Utils\AssertionException $e) {
			throw $this->error($e->getMessage(), 400);
		}

		$test = $this->model->getTest($user, (int) $id);
		if ($test === null) {
			throw $this->error('Test not found.', 404);
		}

		$clusters = $this->embryoModel->getClusters();
		$clustersByCatiId = Arrays::associate(Arrays::map($clusters, function (Cluster $cluster) {
			return [
				'catiId' => $cluster->getCatiId(),
				'cluster' => $cluster,
			];
		}), 'catiId=cluster');

		$embryoIds = $test->getQuestions()->map(function (TestQuestion $question) {
			return $question->getEmbryoId();
		})->getValues();
		$embryos = $this->embryoModel->getEmbryosByIds($embryoIds);

		if (count($embryos) !== $test->getQuestions()->count()) {
			$missingEmbryos = array_diff($embryoIds, array_keys($embryos));
			$message = sprintf('Embryos %s not found.', implode(', ', Arrays::map($missingEmbryos, function ($id) {
				return sprintf('#%d', $id);
			})));
			$this->logger->log(new \RuntimeException($message), ILogger::EXCEPTION);
			throw $this->error($message, 404);
		}

		$missingClusters = array_diff(array_column($embryos, 'CurveClass'), array_keys($clustersByCatiId));
		if (count($missingClusters) > 0) {
			$message = sprintf('Clusters %s not found.', implode(', ', Arrays::map($missingClusters, function ($id) {
				return sprintf('#%d', $id);
			})));
			$this->logger->log(new \RuntimeException($message), ILogger::EXCEPTION);
			throw $this->error($message, 404);
		}

		$answerClusters = [];
		foreach ($test->getQuestions() as $question) {
			$embryo = $embryos[$question->getEmbryoId()];
			$cluster = $clustersByCatiId[$embryo['CurveClass']];
			$answerClusters[$question->getId()] = $cluster;
		}

		$answerValidator = sprintf('array:%d', $test->getQuestions()->count());
		$answerData = Netvor\Embryo\Utils\Validator::validateField($data, 'answers', $answerValidator, $errors);

		$answers = [];
		$questionIds = $test->getQuestions()->map(function (TestQuestion $question) {
			return $question->getId();
		})->getValues();
		$level = $test->getLevel();
		foreach ($answerData ?? [] as $answerRow) {
			$questionId = Netvor\Embryo\Utils\Validator::validateField($answerRow, 'questionId', [$questionIds], $errors);
			if ($questionId === null) {
				continue;
			}
			$cluster = $answerClusters[$questionId];
			$options = TestQuestion::getOptionsByCluster($clusters, $cluster, $level);
			$answerOptions = Arrays::map($options, function (Cluster $option) use ($level) {
				return substr($option->getClassification(), 0, TestQuestion::CLASSIFICATION_LENGTHS[$level]);
			});
			$answer = Netvor\Embryo\Utils\Validator::validateField($answerRow, 'answer', [$answerOptions], $errors);
			if ($answer !== null) {
				$answers[$questionId] = $answer;
			}
		}

		if (!empty($errors)) {
			throw $this->error(implode("\n", $errors), 400);
		}

		if (count($answers) !== $test->getQuestions()->count()) {
			$missingAnswers = array_diff($questionIds, array_keys($answers));
			$message = sprintf('Answers for questions %s are missing.', implode(', ', Arrays::map($missingAnswers, function ($id) {
				return sprintf('#%d', $id);
			})));
			throw $this->error($message, 400);
		}

		$this->model->takeTest($test, $answers, $answerClusters);
		$correct = $test->getQuestions()->filter(function (TestQuestion $question) {
			return $question->isCorrect();
		})->count();

		return $this->json([
			'testId' => $test->getId(),
			'level' => $level,
			'accuracyPercentage' => floor($correct / $test->getQuestions()->count() * 100),
			'correctAnswers' => $correct,
			'success' => $test->isPassed() === true,
			'answeredQuestions' => $test->getQuestions()->map(function (TestQuestion $question) use ($answerClusters, $level) {
				$cluster = $answerClusters[$question->getId()];
				return [
					'questionId' => $question->getId(),
					'success' => $question->isCorrect(),
					'correct' => substr($cluster->getClassification(), 0, TestQuestion::CLASSIFICATION_LENGTHS[$level]),
				];
			})->getValues(),
		]);
	}


	private function formatOptions(array $options, string $level, array $descriptions): array
	{
		return Arrays::map($options, function (Cluster $cluster) use ($level, $descriptions) {
			$description = $this->getClusterDescription($cluster, $level, $descriptions);
			return [
				'value' => substr($cluster->getClassification(), 0, TestQuestion::CLASSIFICATION_LENGTHS[$level]),
				'description' => $description['shortName'] ?? ($level === User::LEVEL_BEGINNER ?
					$cluster->getDevelopmentPhase() :
					$cluster->getShortName()
				),
			];
		});
	}


	private function getClusterDescription(Cluster $cluster, string $level, array $descriptions): ?array
	{
		static $childrenKeys = [
			User::LEVEL_BEGINNER => 'byStage',
			User::LEVEL_INTERMEDIATE => 'byQuality',
			User::LEVEL_EXPERT => 'byEvents',
		];

		$clusterKeys = Arrays::map(TestQuestion::CLASSIFICATION_LENGTHS, function (int $length) use ($cluster) {
			return substr($cluster->getClassification(), 0, $length);
		});

		$current = [$childrenKeys[User::LEVEL_BEGINNER] => $descriptions];
		foreach (TestQuestion::LEVELS as $currentLevel) {
			$childrenKey = $childrenKeys[$currentLevel] ?? null;
			$clusterKey = $clusterKeys[$currentLevel] ?? null;
			if (!isset($childrenKey, $clusterKey, $current[$childrenKey][$clusterKey])) {
				return null;
			}
			$current = $current[$childrenKey][$clusterKey];
			if ($currentLevel === $level) {
				return $current;
			}
		}

		return null;
	}
}
