<?php
declare(strict_types=1);

namespace Netvor\Embryo\ApiModule\Presenters;

use Nette;
use Nette\Application;
use Tracy\ILogger;


class ErrorPresenter implements Application\IPresenter
{
	use Nette\SmartObject;

	/** @var ILogger */
	private $logger;


	public function __construct(ILogger $logger)
	{
		$this->logger = $logger;
	}


	public function run(Application\Request $request): Application\IResponse
	{
		$exception = $request->getParameter('exception');
		$messages = [
			400 => 'Bad Request',
			403 => 'Access Denied',
			404 => 'Page Not Found',
			405 => 'Method Not Allowed',
			410 => 'Page Not Found',
			500 => 'Internal Server Error',
		];

		if (!$exception instanceof Nette\Application\BadRequestException) {
			$this->logger->log($exception, ILogger::EXCEPTION);
		}

		return new Application\Responses\JsonResponse([
			'error' => true,
			'code' => $code = $exception instanceof Application\BadRequestException ? ($exception->getCode() ?: 404) :
				500,
			'message' => $messages[$code] ?? null,
		]);
	}
}
