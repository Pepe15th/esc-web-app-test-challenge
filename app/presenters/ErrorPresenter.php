<?php
declare(strict_types=1);

namespace Netvor\Embryo\Presenters;

use Nette;
use Nette\Application\IPresenterFactory;
use Nette\Application\IRouter;
use Nette\Application\Request;
use Nette\Application\Responses;
use Nette\Http;
use Tracy\ILogger;


class ErrorPresenter implements Nette\Application\IPresenter
{
	use Nette\SmartObject;

	/** @var ILogger */
	private $logger;

	/** @var IPresenterFactory */
	private $presenterFactory;

	/** @var IRouter */
	private $router;

	/** @var Http\IRequest */
	private $httpRequest;


	public function __construct(
		ILogger $logger,
		IPresenterFactory $presenterFactory,
		IRouter $router,
		Http\IRequest $httpRequest
	) {
		$this->logger = $logger;
		$this->presenterFactory = $presenterFactory;
		$this->router = $router;
		$this->httpRequest = $httpRequest;
	}


	public function run(Request $request): Nette\Application\IResponse
	{
		$exception = $request->getParameter('exception');

		if (!$exception instanceof Nette\Application\BadRequestException) {
			$this->logger->log($exception, ILogger::EXCEPTION);
		}

		[$module, $name, $sep] = Nette\Application\Helpers::splitName($request->getPresenterName());

		$presenter = $this->getErrorPresenter($request, $module, $name);
		if ($presenter !== null) {
			return new Responses\ForwardResponse($request->setPresenterName($presenter));
		}

		if ($exception instanceof Nette\Application\BadRequestException) {
			$errorPresenter = $module . $sep . 'Error4xx';
			return new Responses\ForwardResponse($request->setPresenterName($errorPresenter));
		}

		return new Responses\CallbackResponse(function () {
			require __DIR__ . '/templates/Error/500.phtml';
		});
	}


	private function getErrorPresenter(Request $request, $defaultModule, $name): ?string
	{
		$originalRequest = $request->getParameter('request') ?: $this->router->match($this->httpRequest);
		if (!$originalRequest instanceof Request) {
			return null;
		}

		[$module, , $sep] = Nette\Application\Helpers::splitName($originalRequest->getPresenterName());
		while ($module !== $defaultModule) {
			$presenter = $module . $sep . $name;
			try {
				$this->presenterFactory->getPresenterClass($presenter);
				return $presenter;
			} catch (Nette\Application\InvalidPresenterException $e) {
				// do nothing
			}
			if ($module === '') {
				break;
			}
			[$module, , $sep] = Nette\Application\Helpers::splitName($module);
		}

		return null;
	}
}
