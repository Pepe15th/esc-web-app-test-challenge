<?php
declare(strict_types=1);

namespace Netvor\Embryo\Presenters;

use Nette\Application\UI\Form;
use Nette\Utils\Arrays;
use Netvor\Embryo\Model\ClinicService;
use Netvor\Embryo\Model\EmailAlreadyInUseException;
use Netvor\Embryo\Model\Entities\Clinic;
use Netvor\Embryo\Model\UserService;


class RegistrationPresenter extends BasePresenter
{

	/** @var UserService @inject */
	public $userModel;

	/** @var ClinicService @inject */
	public $clinicModel;


	protected function createComponentRegistrationForm()
	{
		$form = new Form;

		$form->addProtection('Your session has expired. Please send the form again.');

		$form->addText('firstName', 'First name')
			->setRequired('Please enter your first name');

		$form->addText('lastName', 'Last name')
			->setRequired('Please enter your last name');

		$form->addSelect('clinic', 'Clinic where you work', [
			null => 'I am an independent embryologist',
		] + Arrays::associate(Arrays::map($this->clinicModel->getAll(), function (Clinic $clinic) {
			return [
				'id' => $clinic->getId(),
				'title' => $clinic->getName(),
			];
		}), 'id=title'));

		$form->addText('email', 'E-mail')
			->setRequired('Please enter your E-mail');

		$form->addPassword('password', 'Password')
			->setRequired('Please enter your password');

		$form->addPassword('passwordConfirm', 'Password (again)')
			->setRequired('Please confirm your password')
			->addRule(Form::EQUAL, 'The passwords do not match', $form['password']);
		$form->addCheckbox('agree')
			->setRequired('Please agree to terms and conditions')
			->addRule(Form::EQUAL, 'Please agree to terms and conditions', true);
		$form->addSubmit('submit', 'Submit');
		$form->onSuccess[] = [$this, 'registrationFormSucceeded'];
		return $form;
	}


	public function registrationFormSucceeded(Form $form, $values)
	{
		$clinic = ($values['clinic'] != null) ? $this->clinicModel->get($values['clinic']) : null;
		try {
			$this->userModel->create($values['firstName'], $values['lastName'], $values['email'], $values['password'], $clinic);
		} catch (EmailAlreadyInUseException $e) {
			$form['email']->addError('This email is already in use.');
			return;
		}
		$this->flashMessage('Your account was successfully created.', 'success');
		$this->redirect('Login:');
	}
}
