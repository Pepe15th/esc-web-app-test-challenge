<?php
declare(strict_types=1);

namespace Netvor\Embryo\Forms;

use Nette\Application\UI\Form;


interface IFormFactory
{
	public function create(): Form;
}
