<?php
declare(strict_types=1);

namespace Netvor\Embryo\Mails;

use Nette;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\ITemplateFactory;
use Nette\Mail\IMailer;
use Nette\Mail\Message;


class MailService
{
	use Nette\SmartObject;

	/** @var string */
	private $defaultFrom;

	/** @var IMailer */
	private $mailer;

	/** @var ITemplateFactory */
	private $templateFactory;

	/** @var LinkGenerator */
	private $linkGenerator;


	public function __construct(
		$defaultFrom,
		IMailer $mailer,
		ITemplateFactory $templateFactory,
		LinkGenerator $linkGenerator
	) {
		if (!Nette\Utils\Validators::isEmail((string) $defaultFrom)) {
			throw new \InvalidArgumentException;
		}

		$this->defaultFrom = (string) $defaultFrom;
		$this->mailer = $mailer;
		$this->templateFactory = $templateFactory;
		$this->linkGenerator = $linkGenerator;
	}


	/**
	 * @param string|array $to
	 */
	public function send(string $type, $to, array $parameters = [], $callback = null): void
	{
		$template = $this->createTemplate($type)
			->setParameters($parameters);

		$mail = $this->createMessage($template);

		foreach ((array) $to as $key => $value) {
			$numeric = Nette\Utils\Validators::isNumeric($key);
			$mail->addTo($numeric ? $value : $key, $numeric ? null : $value);
		}

		if ($callback !== null) {
			Nette\Utils\Callback::invoke($callback, $mail);
		}

		$this->mailer->send($mail);
	}


	private function createTemplate($type): Nette\Bridges\ApplicationLatte\Template
	{
		/** @var Nette\Bridges\ApplicationLatte\Template $template */
		$template = $this->templateFactory->createTemplate();
		$template->getLatte()->addProvider('uiControl', $this->linkGenerator);
		$template->setFile(__DIR__ . '/templates/' . $type . '.latte');
		return $template;
	}


	private function createMessage($html): Message
	{
		return (new Message)
			->setFrom($this->defaultFrom)
			->setHtmlBody((string) $html, __DIR__ . '/templates');
	}
}
