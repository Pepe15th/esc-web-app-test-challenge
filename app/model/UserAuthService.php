<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette;


class UserAuthService
{
	use Nette\SmartObject;

	/** @var EntityRepository */
	private $repository;


	public function __construct(EntityManager $entityManager)
	{
		/** @var EntityRepository $userRepository */
		$userRepository = $entityManager->getRepository(Entities\User::class);
		$this->repository = $userRepository;
	}


	public function getByEmail(string $email): ?Entities\User
	{
		$criteria = ['email' => $email];
		return $this->repository->findOneBy($criteria);
	}
}
