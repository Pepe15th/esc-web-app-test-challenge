<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model;

use Kdyby;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette;
use Nette\Utils\Arrays;
use Netvor\Embryo\Model\Entities\Cluster;
use Netvor\Embryo\Model\Entities\TrainingQuestion;
use Netvor\Embryo\Model\Entities\User;


class TrainingService
{
	use Nette\SmartObject;

	public const MIN_QUESTION_COUNT = 15;

	public const MIN_ACCURACY = 60;

	/** @var EntityManager */
	private $entityManager;

	/** @var EntityRepository */
	private $questionRepository;

	/** @var Kdyby\Doctrine\Connection */
	private $catiDatabase;

	/** @var string */
	private $catiVersion;


	public function __construct(EntityManager $entityManager, Kdyby\Doctrine\Connection $catiDatabase, string $catiVersion)
	{
		$this->entityManager = $entityManager;

		/** @var EntityRepository $questionRepository */
		$questionRepository = $this->entityManager->getRepository(TrainingQuestion::class);
		$this->questionRepository = $questionRepository;

		$this->catiDatabase = $catiDatabase;
		$this->catiVersion = $catiVersion;
	}


	public function getQuestion(User $user, int $id): ?TrainingQuestion
	{
		return $this->questionRepository->findOneBy(['id' => $id, 'user' => $user, 'answer' => null]);
	}


	public function getCurrentCount(User $user, ?string $level = null): ?int
	{
		if ($level === null) {
			$level = $user->getNextLevel() ?? User::LEVEL_EXPERT;
		} elseif (!in_array($level, User::LEVELS, true)) {
			throw new \InvalidArgumentException;
		}

		return $this->questionRepository->countBy(['user' => $user, 'level' => $level, 'answer NOT' => null]);
	}


	public function getAccuracy(User $user, ?string $level = null): ?int
	{
		if ($level === null) {
			$level = $user->getNextLevel() ?? User::LEVEL_EXPERT;
		} elseif (!in_array($level, User::LEVELS, true)) {
			throw new \InvalidArgumentException;
		}

		$lastAnswered = $this->questionRepository->findBy(
			['user' => $user, 'level' => $level, 'answer NOT' => null],
			['answeredAt' => 'DESC'],
			self::MIN_QUESTION_COUNT
		);
		$correct = array_filter($lastAnswered, function (TrainingQuestion $question) {
			return $question->isCorrect();
		});
		return count($lastAnswered) > 0 ? (int) floor(count($correct) / count($lastAnswered) * 100) : null;
	}


	/**
	 * @param Cluster[] $clusters
	 */
	public function nextQuestion(array $clusters, User $user, string $level): TrainingQuestion
	{
		if (!in_array($level, TrainingQuestion::LEVELS, true)) {
			throw new \InvalidArgumentException;
		}

		$next = $this->questionRepository->findOneBy(['user' => $user, 'level' => $level, 'answer' => null], ['id' => 'ASC']);
		if ($next) {
			return $next;
		}

		$options = TrainingQuestion::getAllOptions($clusters, $level);
		$nonEmpty = $this->filterOptionsByEmbryoCount($clusters, $options);
		if (!(count($nonEmpty) > 0)) {
			throw new \Exception('No non-empty clusters found.');
		}

		$prefix = array_keys($nonEmpty)[mt_rand(0, count($nonEmpty) - 1)];
		$classification = array_keys($nonEmpty[$prefix])[mt_rand(0, count($nonEmpty[$prefix]) - 1)];
		$cluster = $nonEmpty[$prefix][$classification];

		$question = new TrainingQuestion($user, $this->getRandomEmbryoId($cluster), $level);
		$this->entityManager->persist($question);
		$this->entityManager->flush();

		return $question;
	}


	public function answerQuestion(TrainingQuestion $question, Cluster $cluster, string $answer): void
	{
		$question->answer($answer, $cluster->getClassification());
		$this->entityManager->flush();
	}


	private function filterOptionsByEmbryoCount(array $clusters, array $options)
	{
		$counts = Arrays::associate($this->catiDatabase->executeQuery('
			SELECT COUNT(CATIResultsEmbryos.EmbryoId) count, Embryos.CurveClass cluster_id
			FROM CATIResultsEmbryos
			JOIN Embryos ON (CATIResultsEmbryos.EmbryoId = Embryos.EmbryoId)
			WHERE CATIResultsEmbryos.CATIVersion = :version
			AND Embryos.CurveClass IN (:clusters)
			GROUP BY Embryos.CurveClass
		', [
			'version' => $this->catiVersion,
			'clusters' => Arrays::map($clusters, function (Cluster $cluster) {
				return $cluster->getCatiId();
			}),
		], [
			'clusters' => Kdyby\Doctrine\Connection::PARAM_INT_ARRAY,
		])->fetchAll(), 'cluster_id=count');

		return array_filter(Arrays::map($options, function ($byClassification) use ($counts) {
			return array_filter($byClassification, function (Cluster $cluster) use ($counts) {
				return ($counts[$cluster->getCatiId()] ?? 0) > 0;
			});
		}), 'count');
	}


	private function getRandomEmbryoId(Cluster $cluster): int
	{
		return (int) $this->catiDatabase->executeQuery('
			SELECT CATIResultsEmbryos.EmbryoId
			FROM CATIResultsEmbryos
			JOIN Embryos ON (CATIResultsEmbryos.EmbryoId = Embryos.EmbryoId)
			WHERE CATIResultsEmbryos.CATIVersion = :version
			AND Embryos.CurveClass = :cluster
			ORDER BY RAND()
			LIMIT 1
		', [
			'version' => $this->catiVersion,
			'cluster' => $cluster->getCatiId(),
		])->fetchColumn();
	}
}
