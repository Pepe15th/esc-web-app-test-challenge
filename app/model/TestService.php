<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model;

use Kdyby;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette;
use Nette\Utils\Arrays;
use Netvor\Embryo\Model\Entities\Cluster;
use Netvor\Embryo\Model\Entities\Test;
use Netvor\Embryo\Model\Entities\TestQuestion;
use Netvor\Embryo\Model\Entities\User;


class TestService
{
	use Nette\SmartObject;

	/** @var EntityManager */
	private $entityManager;

	/** @var EntityRepository */
	private $repository;

	/** @var Kdyby\Doctrine\Connection */
	private $catiDatabase;

	/** @var string */
	private $catiVersion;


	public function __construct(EntityManager $entityManager, Kdyby\Doctrine\Connection $catiDatabase, string $catiVersion)
	{
		$this->entityManager = $entityManager;

		/** @var EntityRepository $repository */
		$repository = $this->entityManager->getRepository(Test::class);
		$this->repository = $repository;

		$this->catiDatabase = $catiDatabase;
		$this->catiVersion = $catiVersion;
	}


	public function getTest(User $user, int $id): ?Test
	{
		$level = $user->getNextLevel();
		return $this->repository->findOneBy(['id' => $id, 'user' => $user, 'level' => $level, 'passed' => null]);
	}


	/**
	 * @param Cluster[] $clusters
	 */
	public function fetchOrCreateTest(array $clusters, User $user): ?Test
	{
		$level = $user->getNextLevel();
		if ($level === null) {
			return null;
		}

		$next = $this->repository->findOneBy(['user' => $user, 'level' => $level, 'passed' => null], ['id' => 'ASC']);
		if ($next) {
			return $next;
		}

		return $this->createTest($clusters, $user);
	}


	/**
	 * @param string[] $answers
	 * @param Cluster[] $clusters
	 */
	public function takeTest(Test $test, array $answers, array $clusters): void
	{
		if ($test->isFinished() || $test->getLevel() !== $test->getUser()->getNextLevel()) {
			throw new \InvalidArgumentException;
		}

		if (!Arrays::every($clusters, function ($cluster) {
			return $cluster instanceof Cluster;
		})) {
			throw new \InvalidArgumentException;
		}

		$classifications = Arrays::map($clusters, function (Cluster $cluster) {
			return $cluster->getClassification();
		});

		$test->take($answers, $classifications);

		if ($test->isPassed() === true) {
			$test->getUser()->levelUp();
		}

		$this->entityManager->flush();
	}


	private function createTest(array $clusters, User $user): Test
	{
		$level = $user->getNextLevel();
		if ($level === null) {
			throw new \InvalidArgumentException;
		}

		$options = TestQuestion::getAllOptions($clusters, $level);
		$counts = $this->getEmbryoCounts($clusters);
		if (array_sum($counts) < Test::QUESTION_COUNT) {
			throw new \Exception('Not enough embryos found.');
		}

		$nonEmpty = array_filter(Arrays::map($options, function ($byClassification) use ($counts) {
			return array_filter($byClassification, function (Cluster $cluster) use ($counts) {
				return ($counts[$cluster->getCatiId()] ?? 0) > 0;
			});
		}), 'count');
		$embryoIds = $used = [];

		for ($i = 0; $i < Test::QUESTION_COUNT; $i++) {
			$prefix = array_keys($nonEmpty)[mt_rand(0, count($nonEmpty) - 1)];
			$classification = array_keys($nonEmpty[$prefix])[mt_rand(0, count($nonEmpty[$prefix]) - 1)];
			$cluster = $nonEmpty[$prefix][$classification];

			$embryoId = $this->getRandomEmbryoId($cluster, $used[$cluster->getClassification()] ?? []);
			$embryoIds[] = $used[$cluster->getClassification()][] = $embryoId;
			$counts[$cluster->getCatiId()]--;
			if ($counts[$cluster->getCatiId()] <= 0) {
				unset($nonEmpty[$prefix][$classification]);
				if (count($nonEmpty[$prefix]) <= 0) {
					unset($nonEmpty[$prefix]);
				}
			}
		}

		$test = new Test($user, $embryoIds);
		$this->entityManager->persist($test);
		$this->entityManager->flush();

		return $test;
	}


	private function getEmbryoCounts(array $clusters)
	{
		return Arrays::associate($this->catiDatabase->executeQuery('
			SELECT COUNT(CATIResultsEmbryos.EmbryoId) count, Embryos.CurveClass cluster_id
			FROM CATIResultsEmbryos
			JOIN Embryos ON (CATIResultsEmbryos.EmbryoId = Embryos.EmbryoId)
			WHERE CATIResultsEmbryos.CATIVersion = :version
			AND Embryos.CurveClass IN (:clusters)
			GROUP BY Embryos.CurveClass
		', [
			'version' => $this->catiVersion,
			'clusters' => Arrays::map($clusters, function (Cluster $cluster) {
				return $cluster->getCatiId();
			}),
		], [
			'clusters' => Kdyby\Doctrine\Connection::PARAM_INT_ARRAY,
		])->fetchAll(), 'cluster_id=count');
	}


	private function getRandomEmbryoId(Cluster $cluster, array $used): int
	{
		$query = '
			SELECT CATIResultsEmbryos.EmbryoId
			FROM CATIResultsEmbryos
			JOIN Embryos ON (CATIResultsEmbryos.EmbryoId = Embryos.EmbryoId)
			WHERE CATIResultsEmbryos.CATIVersion = :version
			AND Embryos.CurveClass = :cluster
		';
		if (count($used) > 0) {
			$query .= "\t" . 'AND CATIResultsEmbryos.EmbryoId NOT IN (:used)' . "\n\t\t";
		}
		$query .= "\t" . 'ORDER BY RAND() LIMIT 1' . "\n\t\t";

		return (int) $this->catiDatabase->executeQuery($query, [
			'version' => $this->catiVersion,
			'cluster' => $cluster->getCatiId(),
			'used' => $used,
		], [
			'used' => Kdyby\Doctrine\Connection::PARAM_INT_ARRAY,
		])->fetchColumn();
	}
}
