<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model\Entities;

use Nette;


/**
 * @property-read int $id
 * @property-read Cluster $cluster
 * @property-read int $offsetMinutes
 */
class Embryo
{
	use Nette\SmartObject;

	/** @var int */
	private $id;

	/** @var Cluster */
	private $cluster;

	/** @var int */
	private $offsetMinutes;


	public function __construct(Cluster $cluster, int $offsetMinutes)
	{
		$this->cluster = $cluster;
		$this->offsetMinutes = $offsetMinutes;
	}


	public function getId(): int
	{
		return $this->id;
	}


	public function getCluster(): Cluster
	{
		return $this->cluster;
	}


	public function getOffsetMinutes(): int
	{
		return $this->offsetMinutes;
	}
}
