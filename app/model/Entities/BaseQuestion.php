<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette;
use Nette\Utils\Arrays;


/**
 * @ORM\MappedSuperclass
 * @property-read ?int $id
 * @property-read int $embryoId
 * @property-read string $level one of the BaseQuestion::LEVELS
 * @property-read ?string $answer
 * @property-read ?bool $correct
 * @property-read bool $answered
 */
abstract class BaseQuestion
{
	use Nette\SmartObject;

	public const LEVELS = [
		User::LEVEL_BEGINNER,
		User::LEVEL_INTERMEDIATE,
		User::LEVEL_EXPERT,
	];

	public const USER_LEVELS = [
		User::LEVEL_BEGINNER => [User::LEVEL_NEW_USER, User::LEVEL_BEGINNER, User::LEVEL_INTERMEDIATE, User::LEVEL_EXPERT],
		User::LEVEL_INTERMEDIATE => [User::LEVEL_BEGINNER, User::LEVEL_INTERMEDIATE, User::LEVEL_EXPERT],
		User::LEVEL_EXPERT => [User::LEVEL_INTERMEDIATE, User::LEVEL_EXPERT],
	];

	public const CLASSIFICATION_LENGTHS = [
		User::LEVEL_BEGINNER => 1,
		User::LEVEL_INTERMEDIATE => 2,
		User::LEVEL_EXPERT => 3,
	];

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var ?int
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $embryoId;

	/**
	 * @ORM\Column(nullable=true)
	 * @var ?string
	 */
	private $answer;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 * @var ?bool
	 */
	private $correct;


	public function __construct(int $embryoId)
	{
		$this->embryoId = $embryoId;
	}


	public function __clone()
	{
		$this->id = null;
	}


	public function getId(): ?int
	{
		return $this->id;
	}


	abstract public function getUser();


	public function getEmbryoId(): int
	{
		return $this->embryoId;
	}


	/**
	 * @return string one of the BaseQuestion::LEVELS
	 */
	abstract public function getLevel(): string;


	public function getAnswer(): ?string
	{
		return $this->answer;
	}


	public function isCorrect(): ?bool
	{
		return $this->correct;
	}


	public function isAnswered(): bool
	{
		return $this->answer !== null;
	}


	/**
	 * @return $this
	 */
	public function answer(string $answer, string $correctClassification)
	{
		$this->answer = $answer;
		$this->correct = strncmp($this->answer, $correctClassification, self::CLASSIFICATION_LENGTHS[$this->level]) === 0;
		return $this;
	}


	/**
	 * @param Cluster[] $clusters
	 * @param string $level one of the BaseQuestion::LEVELS
	 * @return Cluster[][]
	 */
	public static function getAllOptions(array $clusters, string $level): array
	{
		if (!isset(self::CLASSIFICATION_LENGTHS[$level])) {
			throw new \InvalidArgumentException;
		}

		$classificationLength = self::CLASSIFICATION_LENGTHS[$level];

		/** @var Cluster[][] $byPrefix */
		$byPrefix = Arrays::associate(Arrays::map($clusters, function (Cluster $cluster) use ($classificationLength) {
			return [
				'prefix' => substr($cluster->getClassification(), 0, $classificationLength - 1),
				'classification' => substr($cluster->getClassification(), 0, $classificationLength),
				'cluster' => $cluster,
			];
		}), 'prefix|classification[]=cluster');

		return Arrays::map(array_filter($byPrefix, function (array $byClassification) {
			return count($byClassification) > 1;
		}), function (array $byClassification) {
			return Arrays::map($byClassification, function (array $classificationClusters) {
				return array_reduce($classificationClusters, function (?Cluster $min, Cluster $cluster) {
					return $min === null || $cluster->getClassification() < $min->getClassification() ? $cluster : $min;
				});
			});
		});
	}


	/**
	 * @param Cluster[] $clusters
	 * @return Cluster[]
	 */
	public static function getOptionsByCluster(array $clusters, Cluster $cluster, string $level): array
	{
		if (!isset(self::CLASSIFICATION_LENGTHS[$level])) {
			throw new \InvalidArgumentException;
		}

		$options = self::getAllOptions($clusters, $level);
		$prefix = substr($cluster->getClassification(), 0, self::CLASSIFICATION_LENGTHS[$level] - 1);
		return array_values($options[$prefix] ?? [$cluster]);
	}
}
