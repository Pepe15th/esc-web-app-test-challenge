<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model\Entities;

use Nette;


/**
 * @property-read int $id
 * @property-read int $classification
 * @property-read string $name
 */
class DevelopmentPhase
{
	use Nette\SmartObject;

	/** @var int */
	private $id;

	/** @var int */
	private $classification;

	/** @var string */
	private $name;


	public function __construct(int $classification, string $name)
	{
		$this->classification = $classification;
		$this->name = $name;
	}


	public function getId(): int
	{
		return $this->id;
	}


	public function getClassification(): int
	{
		return $this->classification;
	}


	public function getName(): string
	{
		return $this->name;
	}
}
