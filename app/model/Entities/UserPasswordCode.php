<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette;
use Nette\Security\Passwords;


/**
 * @ORM\Entity
 * @property-read ?int $id
 * @property-read User $user
 * @property-read Nette\Utils\DateTime $createdAt
 */
class UserPasswordCode
{
	use Nette\SmartObject;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 * @var ?int
	 */
	private $id;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="User")
	 * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
	 */
	private $user;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	private $codeHash;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	private $created;


	public function __construct(User $user, string $code)
	{
		$this->user = $user;
		$this->codeHash = Passwords::hash($code);
		$this->created = new Nette\Utils\DateTime;
	}


	public function __clone()
	{
		$this->id = null;
	}


	public function getId(): ?int
	{
		return $this->id;
	}


	public function getUser(): User
	{
		return $this->user;
	}


	public function getCreated(): Nette\Utils\DateTime
	{
		return Nette\Utils\DateTime::from($this->created);
	}


	public function verifyCode($code): bool
	{
		return Passwords::verify($code, $this->codeHash);
	}
}
