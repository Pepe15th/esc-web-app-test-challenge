<?php
declare(strict_types=1);

namespace Netvor\Embryo\Components;


interface IContactControlFactory
{
	public function create(): ContactControl;
}
