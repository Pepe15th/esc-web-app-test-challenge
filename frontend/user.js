import React from 'react';
import $ from 'jquery';
import { render } from 'react-dom';
import { StaticRouter } from 'react-router-dom';
import 'bootstrap';
import 'font-awesome/scss/font-awesome.scss';

import Header from './components/Header';
import './forms';
import './index.scss';
import './user.scss';

const context = {
	set url(url) {
		window.location.href = url;
	}
};

// make change password box in user setting section unexpandable
$(function passwordUnaxpandable() {
	let isChecked = false;
	$('input[type=checkbox').on('click', function (e) {
		const checkbox = $(this);
		if (isChecked) {
			if (!checkbox.is(":checked")) {
				e.preventDefault();
				return false;
			}
		} else {
			isChecked = true;
		}
	});
});

const root = document.querySelector('#header-root');

render(
	(
		<StaticRouter basename={`${process.env.PUBLIC_URL}/app`} location={window.location} context={context}>
			<Header userName={root && root.dataset && root.dataset.userName} />
		</StaticRouter>
	),
	root
);
