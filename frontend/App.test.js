import React from 'react';
import ReactDOM from 'react-dom';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import App from './App';
import store from './store';

it('renders without crashing', () => {
	const div = document.createElement('div');
	ReactDOM.render(
		<Provider store={store}>
			<StaticRouter
				location={`/embryo_library`}
				basename={`${process.env.PUBLIC_URL}/app`}
				context={{}}
			>
				<App userName="Name Surname" />
			</StaticRouter>
		</Provider>,
		div,
	);
});
