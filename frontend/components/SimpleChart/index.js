import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import zipWith from 'lodash/zipWith';
import range from 'lodash/range';
import { createSelector } from 'reselect';

import DataChart from './components/DataChart';
import './styles.scss';

class SimpleChart extends PureComponent {

	// Selectors
	getMeasurementCount = (props) => props.measurementCount;
	getOffset = (props) => props.offset;
	getActivity = (props) => props.activityData;
	getExpansion = (props) => props.expansionData;

	getGraphData = createSelector(
		this.getMeasurementCount,
		this.getOffset,
		this.getActivity,
		this.getExpansion,
		(cnt, offset, activity, expansion) => {
			const xRange = range(cnt);
			const intro = range(offset).map(x => ({ x }));

			const values = zipWith(
				xRange,
				activity,
				expansion,
				(x, a, e) => ({ x: offset + x, activity: a, expansion: e }),
			);

			return [
				...intro,
				...values,
			];
		},
	);

	render() {
		const graphData = this.getGraphData(this.props);

		return (
			<DataChart
				data={graphData}
				dataKey1="activity"
				dataKey2="expansion"
				areaColor="#B7E2F5"
				lineColor="#7BCBF1"
				margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
				width={150}
				height={40}
			/>
		);
	}

}

SimpleChart.propTypes = {
	measurementCount: PropTypes.number.isRequired,
	offset: PropTypes.number,
	activityData: PropTypes.array.isRequired,
	expansionData: PropTypes.array.isRequired,
};

SimpleChart.defaultProps = {
	offset: 0,
};

export default SimpleChart;
