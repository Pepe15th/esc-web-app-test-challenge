import React, { Component } from 'react';
import PropTypes from 'prop-types';

import range from 'lodash/range';
import zipWith from 'lodash/zipWith';
import { createSelector } from 'reselect';

import DataChart from './components/DataChart';

const divisionLabelSpread = 3;


class EmbryosChart extends Component {

	// Selectors
	getMeasurementCount = (props) => props.measurementCount;
	getOffset = (props) => props.offset;
	getTotalCount = (props) => props.totalCount || (props.measurementCount + props.offset);
	getExpansion = (props) => props.expansionData;
	getActivity = (props) => props.activityData;
	getEvents = (props) => props.events;

	getGraphData = createSelector(
		this.getMeasurementCount,
		this.getOffset,
		this.getTotalCount,
		this.getExpansion,
		this.getActivity,
		this.getEvents,
		(cnt, offset, totalCnt, expansion, activity, events) => {
			const xRange = range(cnt <= totalCnt - offset ? cnt : totalCnt - offset);
			const intro = range(offset).map(x => ({ x }));
			const outro = cnt < totalCnt - offset ?
				range(totalCnt - offset - cnt).map(x => ({ x: offset + cnt + x })) :
				[];

			const divisions = xRange.map(x => {
				const current = (events[x] || []).filter((event) => event.type === 'division');
				return current.length ? Math.max(...current.map(({ cell_count }) => cell_count)) : undefined;
			});

			const evenDivisions = divisions.map(x => x % 2 === 0 ? x : undefined);

			const divisionLabels = xRange.reduce(({ data, last, delayedValues }, x) => {
				if (last !== null && last + divisionLabelSpread > x) {
					return {
						data: data.concat(undefined),
						last,
						delayedValues: evenDivisions[x] ? delayedValues.concat(evenDivisions[x]) : delayedValues,
					};
				}

				if (delayedValues.length) {
					const [value, ...rest] = delayedValues;
					return {
						data: data.concat(value),
						last: x,
						delayedValues: rest,
					};
				}

				return {
					data: data.concat(evenDivisions[x]),
					last: divisions[x] ? x : last,
					delayedValues,
				};
			}, { data: [], last: null, delayedValues: [1] }).data;

			const cellCounts = divisions.reduce(({ data, count }, currentCount) => {
				const newCount = currentCount || count;

				return {
					data: [...data, newCount],
					count: newCount,
				};
			}, { data: [], count: 1 }).data;

			const values = zipWith(
				xRange,
				expansion,
				activity,
				cellCounts,
				divisionLabels,
				(x, e, a, c, d) => ({ x: offset + x, expansion: e, activity: a, cellCount: c, division: d }),
			);

			return [
				...intro,
				...values,
				...outro,
			];
		},
	);

	getChartEvents = createSelector(
		this.getOffset,
		this.getEvents,
		(offset, events) => {
			return [].concat(...Object.keys(events).map((index) => {
				return (events[index]).filter(({ type }) => {
					return type === 'expansion' || type === 'collapse';
				}).map(({ type, collapseType }) => {
					return type === 'collapse' ?
						{ type, collapseType, index: offset + (+index) } :
						{ type, index: offset + (+index) };
				});
			}));
		},
	);

	render() {
		const graphData = this.getGraphData(this.props);
		const chartEvents = this.getChartEvents(this.props);

		return (
			<DataChart
				data={graphData}
				chartEvents={chartEvents}
				dataKey1="expansion"
				dataKey2="activity"
				dataKey3="cellCount"
				areaColor="#B7E2F5"
				lineColor="#7BCBF1"
				embryoReferenceLine={this.props.embryoReferenceLine}
				onReferenceLineChange={this.props.onReferenceLineChange}
			/>
		);
	}

}

EmbryosChart.propTypes = {
	measurementCount: PropTypes.number.isRequired,
	offset: PropTypes.number,
	totalCount: PropTypes.number,

	expansionData: PropTypes.array.isRequired,
	activityData: PropTypes.array.isRequired,
	events: PropTypes.object.isRequired,

	embryoReferenceLine: PropTypes.number.isRequired,
	onReferenceLineChange: PropTypes.func.isRequired,
};

EmbryosChart.defaultProps = {
	offset: 0,
	totalCount: null,
};


export default EmbryosChart;
