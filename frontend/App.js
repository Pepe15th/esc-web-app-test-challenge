import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './App.scss';
import 'font-awesome/scss/font-awesome.scss';

import Header from './components/Header';
import MainRouter from './components/MainRouter';

class App extends Component {

	render() {
		return (
			<div className="container-fluid">
				<Header
					userName={this.props.userName}
					cleanCacheButton={this.props.cleanCacheButton}
				/>
				<MainRouter />
			</div>
		);
	}

}

App.propTypes = {
	userName: PropTypes.string,
	cleanCacheButton: PropTypes.bool,
};

export default App;
