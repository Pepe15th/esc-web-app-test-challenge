import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import screen1 from './images/screen1.jpg';
import screen2 from './images/screen2.jpg';
import screen3 from './images/screen3.jpg';
import screen4 from './images/screen4.jpg';
import screen8 from './images/screen8.jpg';


export default class ShowcaseOnRealData extends PureComponent {

	static propTypes = {
		onVisitPage: PropTypes.func.isRequired,
	};

	componentDidMount() {
		this.props.onVisitPage();
	}

	render() {
		return (
			<div className="col content">
				<div className="head">
					<div id="go-registration">
						<span>Do you like what you see?</span>
						<Link to="/registration" className="btn btn-orange btn-regi">Register</Link>
					</div>

					<h1>Showcase on real data</h1>
					<p>You have finally arrived to the end of the tutorial. This section shows how ESCAPE looks inside. We put in sample embryo which you can explore. Go ahead, look at the values, try to understand how it works and play around. You should be ready and understand what we propose. If you like what you see, go ahead and register. Try other embryos, study and get the most out of it.</p>
				</div>
				<hr />

				<div className="box">
					<div className="sh-box">
						<div className="inner">
							<div className="embryo-class"><span className="l-h tooltip-top tooltip-top__large tooltip-netvor" data-tooltip-large="true" data-tooltip="Hatching blastocyst with early onset of expansion">1aa</span></div>
						</div>
						<hr />

						<div className="inner">
							<img src={screen8} alt="nahradit grafem" style={{ 'max-width': '100%', }} />
						</div>
					</div>

					<div className="box">
						<div className="hr-tit" data-content="Cleavages" />
						<div className="sh-box">
							<img src={screen1} alt="nahradit grafem" style={{ 'max-width': '100%', }} />
						</div>

						<div className="hr-tit" data-content="Mytotic cycles" />
						<div className="sh-box">
							<img src={screen2} alt="nahradit grafem" style={{ 'max-width': '100%', }} />
						</div>

						<div className="hr-tit" data-content="Collapses" />
						<div className="sh-box">
							<img src={screen3} alt="nahradit grafem" style={{ 'max-width': '100%', }} />
						</div>

						<div className="hr-tit" data-content="Expansions and compaction" />
						<div className="sh-box">
							<img src={screen4} alt="nahradit grafem" style={{ 'max-width': '100%', }} />
						</div>
					</div>
				</div>
			</div>
		);
	}

}
