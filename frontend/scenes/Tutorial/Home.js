import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import screen5 from './images/screen5.jpg';
import screen6 from './images/screen6.jpg';
import screen7 from './images/screen7.jpg';


export default class Home extends PureComponent {

	static propTypes = {
		onVisitPage: PropTypes.func.isRequired,
	};

	componentDidMount() {
		this.props.onVisitPage();
	}

	render() {
		return (
			<div className="col content">
				<div className="head">
					<h1>How does it work</h1>
					<div className="img" >
						<img src="" alt="#" />
					</div>
					<p>The following table shows the ideal embryo for implantation - the actual output of our AI based evaluation. The AI we developed was able to gather and evaluate presented information. Our system is data based - we evaluate embryos by hard data, visual part of embryo is not the key indicator of its quality.</p>
					<p>Please, look at the table and see what we have to offer. To make sure you understand everything properly, there is tutorial waiting for you in the next chapter.</p>

					<div className="embryo-class"><span className="l-h tooltip-top tooltip-top__large tooltip-netvor" data-tooltip-large="true" data-tooltip="Hatching blastocyst with early onset of expansion">1aa</span></div>
					<div className="cluster-id">4318</div>
					<div className="clearfix" />
				</div>

				<div className="box">
					<div className="hr-tit" data-content="Events" />
					<div className="sh-box">
						<img src={screen5} alt="nahradit grafem" style={{ 'max-width': '100%', }} />
					</div>
					<div className="hr-tit" data-content="Mytotic cycles" />
					<div className="sh-box">
						<img src={screen6} alt="nahradit grafem" style={{ 'max-width': '100%', }} />
					</div>
					<div className="hr-tit" data-content="cleavages and cleavage intervals" />
					<div className="sh-box">
						<img src={screen7} alt="nahradit grafem" style={{ 'max-width': '100%', }} />
					</div>
				</div>
			</div>
		);
	}

}
