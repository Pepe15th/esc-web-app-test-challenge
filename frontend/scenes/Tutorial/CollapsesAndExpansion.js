import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { fetchEmbryoDetail } from '../../services/api';

import ArrowDownGraph from '../../components/ArrowDownGraph';
import ArrowUpGraph from '../../components/ArrowUpGraph';
import EmbryoImages from './EmbryoImages';
import Timeline from './Timeline';


export default class CollapsesAndExpansion extends PureComponent {

	static propTypes = {
		onVisitPage: PropTypes.func.isRequired,
	};

	state = {
		embryo: null,
	};

	componentDidMount() {
		this.props.onVisitPage();

		fetchEmbryoDetail(4432).then(data => {
			this.setState({
				embryo: data.embryo,
			});
		});
	}

	render() {
		return (
			<div className="col content" id="first-part-show">
				<div className="head">
					<h1>Collapses and expansion</h1>
					<div className="box-group links">
						<div className="sh-box info-wrap active">
							<ArrowUpGraph color="#A75FD2" />
							<div className="info-text-wrapper">
								<p className="heading">
									Start of expansion
								</p>
								<p className="text">
									Purple arrow marks <span className="bolder-text">where embryo expansion starts</span>.
								</p>
							</div>
						</div>

						<div className="sh-box info-wrap">
							<ArrowDownGraph color="#61B945" />
							<div className="info-text-wrapper">
								<p className="heading">
									Green collapse
								</p>
								<p className="text">
									<span className="bolder-text">Very mild</span> collapses or mitotic pulses – correspond with the group cell cleavages
								</p>
							</div>
						</div>

						<div className="sh-box info-wrap">
							<ArrowDownGraph color="#39A4CD"/>
							<div className="info-text-wrapper">
								<p className="heading">
									Blue collapse
								</p>
								<p className="text">
									<span className="bolder-text">Mild</span> collapses or mitotic pulses – correspond with the group cell cleavages
								</p>
							</div>
						</div>

						<div className="sh-box info-wrap">
							<ArrowDownGraph color="#D7D721"/>
							<div className="info-text-wrapper">
								<p className="heading">
									Yellow collapse
								</p>
								<p className="text">
									<span className="bolder-text">More extensive</span> collapses or mitotic pulses of otherwise correctly cleaving embryos – correspond with the group cell cleavages
								</p>
							</div>
						</div>

						<div className="sh-box info-wrap">
							<ArrowDownGraph color="#E13E52"/>
							<div className="info-text-wrapper">
								<p className="heading">
									Red collapse
								</p>
								<p className="text">
									<span className="bolder-text">Extensive</span> collapses induced by apoptotic cells elimination – the consequence of the previous abnormal cleavages
								</p>
							</div>
						</div>
					</div>
				</div>

				<div className="full">
					{this.state.embryo ? (
						<EmbryoImages embryo={this.state.embryo} />
					) : null}

					<Timeline
						embryo={this.state.embryo}
						leftOffset={0}
					/>
				</div>
			</div>
		);
	}

}
