import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactResizeDetector from 'react-resize-detector';

import './index.scss';


export const FRAMES_COUNT = 7;
export const STEP = 30; // 30 min


class EmbryoImages extends Component {

	static propTypes = {
		embryo: PropTypes.object.isRequired,
		selectedReferenceLine: PropTypes.number.isRequired,
	};

	state = {
		width: -1,
	};

	handleResize = (width) => {
		this.setState({ width });
	};

	render() {
		const embryo = this.props.embryo;
		const stepsOffset = Math.round(embryo.image_offset_minutes / STEP);
		const totalSteps = stepsOffset + (+embryo.image_count) - 1;
		const currentStep = this.props.selectedReferenceLine;

		const width = this.state.width;
		const widthOfStep = width / totalSteps;

		const images = [];
		const getImagePosition = (selectedStep) => {
			return selectedStep < stepsOffset ? 0 : (selectedStep - stepsOffset) / (embryo.image_count - 1) * 100;
		};

		for (let i = 1; i <= FRAMES_COUNT; i++) {
			const lastStepOfThisFrame = Math.floor(totalSteps * i / FRAMES_COUNT);

			const image = <div key={i} className="bg-img" style={{
				backgroundImage: `url(${embryo.image_url})`,
				backgroundPositionX: `${getImagePosition(lastStepOfThisFrame)}%`
			}} />;

			images.push(image);
		}

		const halfFrame = width / FRAMES_COUNT / 2;
		const distanceFromStart = widthOfStep * currentStep;
		const distanceToEnd = width - distanceFromStart;

		const negativeLeftOffset = Math.min(distanceFromStart, Math.max(halfFrame, (2 * halfFrame) - distanceToEnd));

		return (
			<React.Fragment>
				<ReactResizeDetector handleWidth onResize={this.handleResize} />
				<div className='embryo-images'>
					<div className="bg-img active" style={{
						backgroundImage: `url(${embryo.image_url})`,
						backgroundPositionX: `${getImagePosition(currentStep)}%`,
						left: `${-negativeLeftOffset + distanceFromStart}px`
					}} />

					{images}
				</div>
			</React.Fragment>
		);
	}

}


const mapStateToProps = (state) => ({
	selectedReferenceLine: state.embryosTutorialReducer.selectedReferenceLine,
});

export default connect(mapStateToProps)(EmbryoImages);
