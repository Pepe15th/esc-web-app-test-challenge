import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import EmbryoDetail from '../../../../components/EmbryoDetail';

import StageSelector from '../../components/StageSelector';
import QualitySelector from '../../components/QualitySelector';

import EmbryosTable from '../../components/EmbryosTable';

import { actions } from '../../../../reducers/embryos_learning_reducer';

class Events extends Component {

	componentDidMount() {
		this.props.dispatch(actions.visitEmbryosLearning('events'));
	}

	componentWillUnmount() {
		this.props.dispatch(actions.leaveEmbryosLearning('events'));
	}

	onEmbryoEventClick(value) {

		let selectedEmbryoEvent = null;
		const currentEmbryoQuality = this.props.currentEmbryoQuality;

		if(Object.keys(currentEmbryoQuality).length > 0) {

			const clustersByEvents = currentEmbryoQuality.clustersByEvents;

			Object.keys(clustersByEvents).forEach(function (key) {

				if(value === key)
					selectedEmbryoEvent = clustersByEvents[key];

			});

		}

		this.props.dispatch(actions.uiSelectEvent(selectedEmbryoEvent));

	}


	getStagesList() {

		let stages = [];
		const clustersByStage = this.props.clustersByStage;

		Object.keys(clustersByStage).forEach(function (key) {
			stages.push(key);

		});

		return stages;

	}

	getStageValueFromCurrentDevelopmentStageObject() {

		return this.props.currentDevelopmentStage.classification;

	}


	getQualityList() {

		let result = [];
		const currentDevelopmentStage = this.props.currentDevelopmentStage;

		if(Object.keys(currentDevelopmentStage).length > 0) {

			const clustersByQuality = currentDevelopmentStage.clustersByQuality;

			Object.keys(clustersByQuality).forEach(function (key) {
				result.push(key);
			});

		}

		return result;

	}

	getCurrentQualityValue() {

		return this.props.currentEmbryoQuality ? this.props.currentEmbryoQuality.classification.substr(1) : null;

	}


	getEventsList() {

		let result = [];
		const currentEmbryoQuality = this.props.currentEmbryoQuality;

		if(Object.keys(currentEmbryoQuality).length > 0) {

			const clustersByEvents = currentEmbryoQuality.clustersByEvents;

			Object.keys(clustersByEvents).forEach(function (key) {

				result.push(key);

			});

		}

		return result;

	}


	getCurrentEventValue() {

		return this.props.currentEvent ? this.props.currentEvent.classification.substr(2) : null;

	}

	moveReferenceLine = (value) => {
		this.props.dispatch(actions.uiMoveCurrentEventReferenceLine(value));
	};

	render() {

		const clustersByStage = this.props.clustersByStage;

		// Current Development Stage

		const currentDevelopmentStage = this.props.currentDevelopmentStage;
		const stagesList = this.getStagesList();


		let currentSelectedStageValue = null;
		if(stagesList.length > 0) {

			currentSelectedStageValue = (currentDevelopmentStage !== undefined) && (Object.keys(currentDevelopmentStage).length > 0) ?
									this.getStageValueFromCurrentDevelopmentStageObject() :
									stagesList[0];

		}


		// Current Embryo Quality

		let currentEmbryoQuality = this.props.currentEmbryoQuality;
		let currentEvent = this.props.currentEvent;

		// Quality List Values

		const qualityList = this.getQualityList();

		// Current Selected Stage Value

		let currentSelectedQualityValue = null;
		if(qualityList.length > 0) {

			currentSelectedQualityValue = (currentEmbryoQuality !== undefined) && (Object.keys(currentEmbryoQuality).length > 0) ?
									this.getCurrentQualityValue() :
									qualityList[0].substr(1);

		}


		// Events List Values

		const eventsList = this.getEventsList();

		// Current Selected Event Value

		let currentSelectedEventValue = null;

		if(eventsList.length > 0) {

			currentSelectedEventValue = (currentEvent !== undefined) && (Object.keys(currentEvent).length > 0) ?
									this.getCurrentEventValue() :
									eventsList[0].substr(2);

		}


		return (Object.keys(clustersByStage).length > 0) ?

			<section className="learning-main">

				{/* Header */}

				<header>
					<div className="designation-box">

						<StageSelector />
						<QualitySelector />
						<span className="designation-num event-value active">{currentSelectedEventValue || '\u00a0'}</span>

					</div>

					<div className="main-desc">
						<p><b>Third position</b> of cluster mark used in ESCape is <b>event timing</b> of embryo. It is subcategory of quality of embryo.</p>
					</div>

				</header>


				{/* Events Main Section */}

				<section id="main-properties">

					{currentSelectedStageValue < 7 && <h2>Choose event</h2>}
					<div className="inside">

						<div className="designation-box">

							<div className="desc">
								{currentSelectedStageValue < 7 ? 'Timing of a significant event' : 'Clusters of this stage are not classified by event timing'}
							</div>
							{

								currentSelectedStageValue < 7 && eventsList.map((value) => {

										const event = value.substring(2);
										const eventItemClassName = (event === currentSelectedEventValue) ? "designation-num event-value active" : "designation-num event-value";
										const cluster = this.props.currentEmbryoQuality.clustersByEvents[value];

										return <div key={event} className={eventItemClassName} onClick={this.onEmbryoEventClick.bind(this, value)}>
											{event}
											<div className="img">
												<img src={cluster.embryo.last_image_url} alt={`Cluster ${value} embryo`} />
											</div>

										</div>;

								})

							}

						</div>

						{/* Chosen Event */}

						<div className="designation-chosen">

							<div className="designation-num">
								<span>{currentSelectedStageValue}</span>
								<span className="quality-value">{currentSelectedQualityValue}</span>
								<span className="active-color event-value">{currentSelectedEventValue}</span>
							</div>

						</div>

						<h3>
							{currentSelectedStageValue < 7 ? currentDevelopmentStage.name : currentEvent.name}
							{currentSelectedStageValue < 7 && ' '}
							{currentSelectedStageValue < 7 && <span className="active-color">
								{currentEvent.name.substr(currentDevelopmentStage.name.length + 1)}
							</span>}
							<div className="desc">
								{currentDevelopmentStage.shortName}
								{' '}
								<span className={currentEmbryoQuality.shortName !== currentEvent.shortName ? 'active-color' : null}>
									{currentEvent.shortName.substr(currentDevelopmentStage.shortName.length + 1)}
								</span>
							</div>
						</h3>

						<p>{currentEvent.description}</p>

					</div>

					{currentEvent.embryo && <div className="learning-embryo-graph-container">

						<EmbryoDetail
							embryo={currentEvent.embryo}
							current={this.props.currentEventPlayPosition}
							onReferenceLineChange={this.moveReferenceLine}
						/>

					</div>}

					<hr className="learning-divider" />

					<h2 className="learning-table-title">All embryos with clusters
						<div className="desc">arranged by stages with quality and event indicators</div>
					</h2>

					<div className="inside">

						<EmbryosTable />

					</div>

				</section>

			</section>

		: null;

	}

}

const mapStateToProps = (state) => {

	return {

		clustersByStage: state.embryosLearningReducer.data.clustersByStage,

		currentDevelopmentStage: state.embryosLearningReducer.currentDevelopmentStage,
		currentEmbryoQuality: state.embryosLearningReducer.currentEmbryoQuality,

		currentEvent: state.embryosLearningReducer.currentEvent,
		currentEventPlayPosition: state.embryosLearningReducer.currentEventPlayPosition,

	};

};

Events.propTypes = {

	clustersByStage: PropTypes.object,

	currentDevelopmentStage: PropTypes.object,
	currentEmbryoQuality: PropTypes.object,

	currentEvent: PropTypes.object,
	currentEventPlayPosition: PropTypes.number,

	dispatch: PropTypes.func.isRequired

};


export default connect(mapStateToProps)(Events);
