import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Cluster from './Cluster';


import './styles.scss';

const QUALITY_COLUMNS = ['a', 'b', 'c', 'd'];

class EmbryosTable extends Component {

	getRowClassName(stageKey) {
		const clustersByStage = this.props.clustersByStage;
		const stage = clustersByStage && clustersByStage[stageKey];
		const firstStage = clustersByStage && clustersByStage[Object.keys(clustersByStage)[0]];

		const hasCategoryTitle = stage && stage !== firstStage && stage.clustersByQuality &&
			Object.keys(stage.clustersByQuality).reduce((prev, key) => {
				const quality = stage.clustersByQuality[key];
				const category = quality && this.props.clusterCategories && this.props.clusterCategories[quality.category];
				return prev || (category && category.clustersByQuality && category.clustersByQuality[0] === quality.classification);
			}, false);

		return `embryos-table-row${hasCategoryTitle ? ' has-category-title' : ''}`;
	}

	getStatusClassName(status) {
		switch (status) {
			case 'Normal': return 'normal';
			case 'Delayed': return 'delayed';
			case 'Collapses': return 'collapses';
			case 'Low cell count': return 'low';
			case 'Degenerated':
			case 'Arrested':
			case 'Unfertilized': return 'degenerated';
			default: return '';
		}
	}

	renderStatuses() {
		const clustersByStage = this.props.clustersByStage;
		const stage = clustersByStage && clustersByStage[Object.keys(clustersByStage)[0]];
		if (!stage) {
			return null;
		}

		return QUALITY_COLUMNS.map((column) => {
			const classification = `${stage.classification}${column}`;
			const quality = stage.clustersByQuality && stage.clustersByQuality[classification];
			const category = quality && this.props.clusterCategories && this.props.clusterCategories[quality.category];

			const status = category && category.title;
			const className = status && this.getStatusClassName(status);

			return category ? (
				<th style={{width:'199px'}} key={column} className={className}><div>{status}</div></th>
			) : (
				<th style={{width:'199px'}} key={column} />
			);
		});

	}

	renderEmbryos(stageKey) {
		const clustersByStage = this.props.clustersByStage;
		const stage = clustersByStage && clustersByStage[stageKey];
		const firstStage = clustersByStage && clustersByStage[Object.keys(clustersByStage)[0]];

		if (!stage) {
			return null;
		}

		return QUALITY_COLUMNS.map((column) => {
			const classification = `${stage.classification}${column}`;
			const quality = stage.clustersByQuality && stage.clustersByQuality[classification];

			if (!quality) {
				return <td key={column} />;
			}

			const category = this.props.clusterCategories && this.props.clusterCategories[quality.category];
			const status = category && category.title;
			const className = status && this.getStatusClassName(status);

			const firstCluster = category && category.clustersByQuality && category.clustersByQuality[0];
			const categoryTitle = firstCluster && firstCluster === quality.classification && stage !== firstStage ? (
				<div className={`category-title${className ? ` ${className}` : ''}`}>{status}</div>
			) : null;

			const clusters = this.props.events ? (quality.clustersByEvents || {}) : { [classification]: quality };
			const implantationChance = this.props.events && this.props.implantationChance;
			const tooltipClassName = `impl-chance${className ? ` ${className}-bg` : ''}`;

			const columnValues = Object.keys(clusters).map((key) => {
				const embryo = clusters[key].embryo;

				return (
					<Cluster
						key={key}
						clickAble={this.props.clusterClickable}
						developmentStageName={stage.name}
						className={className}
						embryo={embryo}
						tooltipClassName={tooltipClassName}
						implantationChance={implantationChance}
						cluster={clusters[key]}
					/>
				);
			});

			return (
				<td style={{width:'199px'}} key={column}>
					{categoryTitle}
					{columnValues}
				</td>
			);
		});
	}

	renderDevelopmentStages() {
		const clustersByStage = this.props.clustersByStage;

		// Render Development Stages (Rows)
		return Object.keys(clustersByStage).map((key) => {
			const stageKey = key;
			const developmentStageName = clustersByStage[stageKey].name;
			const developmentStageShortName = clustersByStage[stageKey].shortName;

			// Render Row & Each Column

			const embryoColumns = this.renderEmbryos(stageKey);

			const rowClassName = this.getRowClassName(stageKey);
			const divClassName= "tr-tit tr-" + stageKey;

			return (
				<tr key={key} className={rowClassName}>
					<td style={{width:'25%'}}>
						<div className={divClassName}></div>

						<p className="stage-key">
							{stageKey}
						</p>
						<p className="development-stagename">
							{developmentStageName}
						</p>
						<div className="desc">{developmentStageShortName}</div>
					</td>
					{embryoColumns}
				</tr>
			);
		});
	}

	render() {

		const statuses = this.renderStatuses();
		const developmentStages = this.renderDevelopmentStages();

		return (

			<table style={{display:'inline-block'}} className="embryos-table">

				<thead>
					<tr>
						<th className="stage-of-dev-th">Stage of development</th>
						{statuses}
					</tr>
				</thead>

				<tbody>
					{developmentStages}
				</tbody>

			</table>

		);
	}

}


const mapStateToProps = (state) => ({
	clusterCategories: state.embryosLearningReducer.data.clusterCategories,
	clustersByStage: state.embryosLearningReducer.data.clustersByStage,
});

EmbryosTable.propTypes = {
	events: PropTypes.bool,
	implantationChance: PropTypes.bool,
	clusterCategories: PropTypes.object,
	clustersByStage: PropTypes.object,
};

EmbryosTable.defaultProps = {
	events: true,
	implantationChance: false,
};

export default connect(mapStateToProps)(EmbryosTable);
