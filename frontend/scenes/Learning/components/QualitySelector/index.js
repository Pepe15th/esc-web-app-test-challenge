import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { actions } from '../../../../reducers/embryos_learning_reducer';

import './styles.scss';

class QualitySelector extends Component {

	onQualityItemClick(quality) {

		const clustersByQuality = this.props.currentDevelopmentStage.clustersByQuality;
		const qualityObject = clustersByQuality[quality];

		this.props.dispatch(actions.uiSelectQuality(qualityObject));

	}

	getStagesList() {

		let stages = [];
		const clustersByStage = this.props.clustersByStage;

		Object.keys(clustersByStage).forEach(function (key) {
			stages.push(key);

		});

		return stages;

	}

	getStageValueFromCurrentDevelopmentStageObject() {

		return this.props.currentDevelopmentStage.classification;

	}


	getQualityListByStage(stage) {

		let qualities = [];
		const clustersByStage = this.props.clustersByStage;
		const stageObject = clustersByStage[stage];

		const clustersByQuality = stageObject["clustersByQuality"];

		Object.keys(clustersByQuality).forEach(function (key) {

			const quality = key.substr(1);
			qualities.push(quality);

		});

		return qualities;

	}

	render() {

		// Get Stages Values

		const stagesList = this.getStagesList();

		let qualityList = null;
		let currentSelectedStage = null;
		let currentSelectedQuality = null;


		if(stagesList.length > 0) {

			// Get Current Development Stage

			currentSelectedStage = (this.props.currentDevelopmentStage !== undefined && Object.keys(this.props.currentDevelopmentStage).length > 0) ?
									this.getStageValueFromCurrentDevelopmentStageObject() :
									stagesList[0];


			// Get Current Quality Value

			qualityList = this.getQualityListByStage(currentSelectedStage);
			currentSelectedQuality = (this.props.currentEmbryoQuality !== undefined) && (JSON.stringify(this.props.currentEmbryoQuality) !== "{}") ?
									this.props.currentEmbryoQuality.classification.substr(1) :
									qualityList[0];

		}


		return (qualityList !== null) ? (

			<span className="designation-num letter-value changeable">{currentSelectedQuality}<div className="overflow"></div>

				<div className="options">

					{

						qualityList.map(function (value) {

							const quality = currentSelectedStage + value;
							return <span key={value} className="letter-value" onClick={this.onQualityItemClick.bind(this, quality)}>{value}</span>;

						}, this)

					}

				</div>

			</span>

		) : null;

	}

}

const mapStateToProps = (state) => {

	return {

		clustersByStage: state.embryosLearningReducer.data.clustersByStage,
		currentDevelopmentStage: state.embryosLearningReducer.currentDevelopmentStage,
		currentEmbryoQuality: state.embryosLearningReducer.currentEmbryoQuality

	};

};

QualitySelector.propTypes = {

	clustersByStage: PropTypes.object,
	currentDevelopmentStage: PropTypes.object,
	currentEmbryoQuality: PropTypes.object,
	dispatch: PropTypes.func.isRequired

};

export default connect(mapStateToProps)(QualitySelector);
