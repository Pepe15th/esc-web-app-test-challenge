import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

import SuccessRateIcon from '../../../../components/SuccessRateIcon';
import LegendSuccessRate from '../../../../components/LegendSuccessRate';
import EmbryoTrainingIcon from '../../../../components/EmbryoTrainingIcon';


class TrainingRequirements extends Component {

	render() {

		const minAccuracyPercentage = this.props.minAccuracyPercentage;
		const accuracyPercentage = (this.props.accuracyPercentage === null) ? 0 : this.props.accuracyPercentage;

		const currentTrainedEmbryos = this.props.currentTrainedEmbryos;
		const totalEmbryos = this.props.totalEmbryos;

		return (

			<div className="training-requirements-container">

				<h2>Training requirements</h2>

				<div className="training-embryos-number-container">
					<EmbryoTrainingIcon />
					<p>Train on <span>{totalEmbryos} embryos</span>&nbsp;</p>
					<div className="progress-wrapper">
						<div className="progress">
							<div className="progress-bar" style={{width: (163/totalEmbryos)*currentTrainedEmbryos}} role="progressbar" aria-valuenow={currentTrainedEmbryos}
								aria-valuemin="0" aria-valuemax={totalEmbryos}>
							</div>
						</div>
						<p><span>{currentTrainedEmbryos}</span> / {totalEmbryos} </p>
					</div>
				</div>

				<div className="training-embryos-accuracy-container">
					<SuccessRateIcon />
					<p>Achieve <span>{minAccuracyPercentage}% success rate&nbsp;</span></p>
					<LegendSuccessRate lastInstances={totalEmbryos} />
					<div className="progress-wrapper">
						<div className="min-value-wrapper" style={{left: ((163/100)*minAccuracyPercentage - 13)}}>
							<span>MIN</span>
							<span className="arrow" />
						</div>
						<div className="progress">
							<div className="progress-bar" style={{width: (163/100)*accuracyPercentage}} role="progressbar" aria-valuenow={currentTrainedEmbryos}
								aria-valuemin="0" aria-valuemax={totalEmbryos}>
							</div>
						</div>
						<p><span>{accuracyPercentage}%</span></p>
					</div>
				</div>

			</div>

		);

	}

}

TrainingRequirements.propTypes = {

	minAccuracyPercentage: PropTypes.number,
	accuracyPercentage: PropTypes.number,

	currentTrainedEmbryos: PropTypes.number,
	totalEmbryos: PropTypes.number

};

export default TrainingRequirements;
