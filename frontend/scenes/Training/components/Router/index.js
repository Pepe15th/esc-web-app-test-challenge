import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Dashboard from '../../scenes/Dashboard';
import EmbryoStageSelection from '../../scenes/EmbryoStageSelection';
import EmbryosTest from '../../scenes/EmbryosTest';

class Router extends Component {

	render() {

		return(

				<Switch>

					<Route
						exact
						path="/training"
						component={Dashboard}
					/>

					<Route
						path="/training/embryo_stage_selection"
						component={EmbryoStageSelection}
					/>

					<Route
						path="/training/embryos_test"
						component={EmbryosTest}
					/>

				</Switch>

		);

	}


}

export default Router;
