import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { actions } from '../../../../../../../../reducers/embryos_library_reducer';

import RingsWhiteIcon from '../../../../../../../../img/app-icons/icon-comparesmall.svg';
import './styles.scss';


class AddToCompareItem extends Component {

	onCompareItemClick = (e) => {
		e.stopPropagation();

		const { embryo, currentCompareFirstEmbryo, currentCompareSecondEmbryo } = this.props;

		if (embryo.id === currentCompareFirstEmbryo.id) {
			this.props.dispatch(actions.uiRemoveFromCompareFirstEmbryo());
		} else if (embryo.id === currentCompareSecondEmbryo.id) {
			this.props.dispatch(actions.uiRemoveFromCompareSecondEmbryo());
		} else if (!(Object.keys(currentCompareFirstEmbryo).length > 0)) {
			this.props.dispatch(actions.uiAddToCompareFirstEmbryo(embryo));
		} else if (!(Object.keys(currentCompareSecondEmbryo).length > 0)) {
			this.props.dispatch(actions.uiAddToCompareSecondEmbryo(embryo));
		}
	};

	render() {
		const { embryo, currentCompareFirstEmbryo, currentCompareSecondEmbryo } = this.props;

		if (
			Object.keys(currentCompareFirstEmbryo).length > 0 && embryo.id !== currentCompareFirstEmbryo.id &&
			Object.keys(currentCompareSecondEmbryo).length > 0 && embryo.id !== currentCompareSecondEmbryo.id
		) {
			return null;
		}

		return (
			<div className="add-to-compare-button-container">
				<button className="add-to-compare-button" type="button" onClick={this.onCompareItemClick}>
					<img src={RingsWhiteIcon} alt="" />
					<span className="compare">Compare</span>
					<span className="comparing">Comparing</span>
					<i className="fa fa-check" aria-hidden="true" />
					<i className="fa fa-plus" aria-hidden="true" />
				</button>
			</div>
		);
	}

}

AddToCompareItem.propTypes = {
	embryo: PropTypes.object.isRequired,
	currentCompareFirstEmbryo: PropTypes.object.isRequired,
	currentCompareSecondEmbryo: PropTypes.object.isRequired,
	dispatch: PropTypes.func.isRequired,
};


export default connect()(AddToCompareItem);
