import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const LIMIT = 10;


class LoadMore extends PureComponent {

	onLoadMoreClick = () => {
		this.props.loadMore(this.props.category, this.props.cluster, this.props.offset, LIMIT);
	};

	render() {
		const { lastValuesCounter, offset } = this.props;

		if (lastValuesCounter !== undefined ? lastValuesCounter !== LIMIT : (offset % LIMIT) !== 0) {
			 return null;
		}

		return (
			<div className="load-more-container">
				<button className="load-more-button" type="button" onClick={this.onLoadMoreClick}>Load more</button>
			</div>
		);
	}

}

LoadMore.propTypes = {
	category: PropTypes.string.isRequired,
	cluster: PropTypes.string.isRequired,
	offset: PropTypes.number.isRequired,
	lastValuesCounter: PropTypes.number,
	loadMore: PropTypes.func.isRequired,
};

LoadMore.defaultProps = {
	lastValuesCounter: undefined,
};


export default LoadMore;

