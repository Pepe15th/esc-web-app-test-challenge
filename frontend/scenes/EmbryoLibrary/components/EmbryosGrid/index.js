import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import $ from 'jquery';

import { actions } from '../../../../reducers/embryos_library_reducer';

import Embryo from './components/Embryo';
import LoadMore from './components/LoadMore';
import TopBar from './components/TopBar';

import './styles.scss';

const add = (a, b) => a + b;


class EmbryosGrid extends Component {

	componentDidMount() {
		if (window.location.hash.substr(0, 7) === '#embryo') {
			const hashId = +window.location.hash.substr(7);

			const embryosByCategories = this.props.embryosByCategories;
			const categories = Object.keys(embryosByCategories);

			for (let i = 0; i < categories.length; i += 1) {
				const category = embryosByCategories[categories[i]];
				for (let j = 0; j < category.length; j += 1) {
					const cluster = category[j];
					for (let k = 0; k < cluster.values.length; k += 1) {
						const embryo = cluster.values[k];
						if (+embryo.id === hashId) {
							$(window).scrollTop($(window.location.hash).offset().top);
							this.props.dispatch(actions.uiSelectEmbryo(embryo));
							return;
						}
					}
				}
			}
		}
	}

	handleToggleListView = (value) => {
		this.props.dispatch(actions.uiChangeEmbryosListViewToggle(value));
	};

	handleLoadMore = (category, cluster, offset, limit) => {
		this.props.dispatch(actions.requestEmbryosLoadMore(category, cluster, offset, limit));
	};

	isCategoryInFilter(category) {
		const { currentFilteredEmbryos: filter } = this.props;
		return filter === null || (filter[category] || []).length > 0;
	}

	isClusterInFilter(category, { cluster }) {
		const { currentFilteredEmbryos: filter } = this.props;
		return filter === null || (filter[category] || []).indexOf(cluster) >= 0;
	}

	countShownEmbryos() {
		const { embryosByCategories } = this.props;

		return Object.keys(embryosByCategories).map((category) => {
			return embryosByCategories[category].map((cluster) => {
				return this.isClusterInFilter(category, cluster) ? cluster.values.length : 0;
			}).reduce(add, 0);
		}).reduce(add, 0);
	}

	renderCluster(category, cluster) {
		const { listView, currentCompareFirstEmbryo, currentCompareSecondEmbryo, currentSelectedEmbryo } = this.props;

		const colorClass = `cluster-color-${cluster.cluster}`;
		const embryoRowClusterInnerContainerClass = `embryos-row-cluster-inner-container ${colorClass}`;

		return (
			<div className="embryos-row" key={cluster.cluster}>
				<div className="row-top-bar-container">
					<div className="embryos-row-cluster-container">
						<h3 className={colorClass}>{cluster.cluster} - {cluster.learning_name}</h3>
					</div>
				</div>

				<div className={embryoRowClusterInnerContainerClass}>
					{cluster.values.map((value) => {
						// Is Embryo Selected?
						const selected = Object.keys(currentSelectedEmbryo).length > 0 && value.id === currentSelectedEmbryo.id;

						return (
							<Embryo
								key={value.id}
								embryo={value}
								cluster={cluster}
								listView={listView}
								currentCompareFirstEmbryo={currentCompareFirstEmbryo}
								currentCompareSecondEmbryo={currentCompareSecondEmbryo}
								selected={selected}
							/>
						);
					})}

					<div className="clearfix" />

					<LoadMore
						category={category}
						cluster={cluster.cluster}
						offset={cluster.values.length}
						lastValuesCounter={cluster.lastValuesCounter}
						loadMore={this.handleLoadMore}
					/>
				</div>
			</div>
		);
	}

	render() {
		const { embryosByCategories, listView } = this.props;

		return (
			<div className="container-fluid">
				<TopBar
					shownEmbryosCounter={this.countShownEmbryos()}
					listView={listView}
					toggleListView={this.handleToggleListView}
				/>

				<div className="container-fluid">
					{Object.keys(embryosByCategories).map((category) => {
						if (!this.isCategoryInFilter(category)) {
							return null;
						}

						return (
							<div key={category}>
								<div className="embryos-header">
									<span>{category}</span>
								</div>

								{embryosByCategories[category].map((cluster) => {
									if (!(this.isClusterInFilter(category, cluster) && cluster.values.length > 0)) {
										return null;
									}

									return this.renderCluster(category, cluster);
								})}
							</div>
						)
					})}
				</div>
			</div>
		);
	}

}

EmbryosGrid.propTypes = {
	embryosByCategories: PropTypes.object.isRequired,
	listView: PropTypes.bool.isRequired,

	currentCompareFirstEmbryo: PropTypes.object.isRequired,
	currentCompareSecondEmbryo: PropTypes.object.isRequired,

	currentFilteredEmbryos: PropTypes.object,
	currentSelectedEmbryo: PropTypes.object.isRequired,

	dispatch: PropTypes.func.isRequired,
};

EmbryosGrid.defaultProps = {
	currentFilteredEmbryos: null,
};


export default connect()(EmbryosGrid);
