import React, { Component } from 'react';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';

import ArrowToTop from '../../components/ArrowToTop';
import Loading from '../../components/Loading';
import EmbryosComparePreview from './components/EmbryosComparePreview';
import EmbryosFilter from './components/EmbryosFilter';
import EmbryosGrid from './components/EmbryosGrid';

import { actions } from '../../reducers/embryos_library_reducer';

import './styles.scss';

const MAX_EMBRYOS_PER_CLUSTER = 10;


class EmbryoLibrary extends Component {

	componentDidMount() {
		if (Object.keys(this.props.embryosByCategories).length === 0) {
			const hash = window.location.hash;
			const embryoId = (hash.substr(0, 7) === '#embryo' && +hash.substr(7)) || null;
			this.props.dispatch(actions.requestEmbryos(MAX_EMBRYOS_PER_CLUSTER, embryoId));
		}
	}

	render() {
		const { embryosByCategories } = this.props;

		if (Object.keys(embryosByCategories).length === 0) {
			return (
				<div className="container-fluid">
					<Loading />
				</div>
			);
		}

		const {
			listView,
			currentCompareFirstEmbryo,
			currentCompareFirstCategory,
			currentCompareSecondEmbryo,
			currentCompareSecondCategory,
			isCompareCompleteVisible,
			currentFilteredEmbryos,
			currentSelectedEmbryo,
		} = this.props;

		return (
			<div className="embryos-library-container container-fluid">
				<div className="row">
					<div className="col-md-2 no-padding">
						<EmbryosFilter
							embryosByCategories={embryosByCategories}
							currentFilteredEmbryos={currentFilteredEmbryos}
						/>
					</div>

					<div className="col-md-10 Block">
						<EmbryosGrid
							embryosByCategories={embryosByCategories}
							listView={listView}
							currentCompareFirstEmbryo={currentCompareFirstEmbryo}
							currentCompareSecondEmbryo={currentCompareSecondEmbryo}
							currentFilteredEmbryos={currentFilteredEmbryos}
							currentSelectedEmbryo={currentSelectedEmbryo}
						/>
					</div>

					<EmbryosComparePreview
						currentCompareFirstEmbryo={currentCompareFirstEmbryo}
						currentCompareFirstCategory={currentCompareFirstCategory}
						currentCompareSecondEmbryo={currentCompareSecondEmbryo}
						currentCompareSecondCategory={currentCompareSecondCategory}
						isCompareCompleteVisible={isCompareCompleteVisible}
					/>
				</div>

				<ArrowToTop className={isCompareCompleteVisible ? 'arrow-move-up' : null} />
			</div>
		);
	}

}

EmbryoLibrary.propTypes = {
	embryosByCategories: PropTypes.object.isRequired,
	listView: PropTypes.bool.isRequired,

	currentCompareFirstEmbryo: PropTypes.object.isRequired,
	currentCompareFirstCategory: PropTypes.object.isRequired,
	currentCompareSecondEmbryo: PropTypes.object.isRequired,
	currentCompareSecondCategory: PropTypes.object.isRequired,
	isCompareCompleteVisible: PropTypes.bool.isRequired,

	currentFilteredEmbryos: PropTypes.object,
	currentSelectedEmbryo: PropTypes.object.isRequired,

	dispatch: PropTypes.func.isRequired,
};

EmbryoLibrary.defaultProps = {
	currentFilteredEmbryos: null,
};


const mapStateToProps = (state) => {
	return {
		embryosByCategories: state.embryosLibraryReducer.data.embryosByCategories,
		listView: state.embryosLibraryReducer.listView,

		currentCompareFirstEmbryo: state.embryosLibraryReducer.currentCompareFirstEmbryo,
		currentCompareFirstCategory: state.embryosLibraryReducer.currentCompareFirstCategory,
		currentCompareSecondEmbryo: state.embryosLibraryReducer.currentCompareSecondEmbryo,
		currentCompareSecondCategory: state.embryosLibraryReducer.currentCompareSecondCategory,
		isCompareCompleteVisible: state.embryosLibraryReducer.isCompareCompleteVisible,

		currentFilteredEmbryos: state.embryosLibraryReducer.currentFilteredEmbryos,
		currentSelectedEmbryo: state.embryosLibraryReducer.currentSelectedEmbryo,
	};
};

export default connect(mapStateToProps)(EmbryoLibrary);
