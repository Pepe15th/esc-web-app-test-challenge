import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import Tutorial from '../scenes/Tutorial';
import store from '../store';
import registerServiceWorker from '../registerServiceWorker';

const root = document.getElementById('root');

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter basename={`${process.env.PUBLIC_URL}/tutorial`}>
			<Tutorial />
		</BrowserRouter>
	</Provider>,
	root
);

registerServiceWorker();
