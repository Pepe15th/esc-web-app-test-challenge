/* @flow */
import type { TCbEmbryoEvent } from './cb/TCbEmbryoEvent';

export type TEmbryoEvent = {
  measurementIndex: number,
  type: TCbEmbryoEvent
};


