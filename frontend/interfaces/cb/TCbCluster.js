/* @flow */
export type TCbCluster = {
  id: string,
  curveClassID: string,
  curveClassDesc: string,
  classification: string,
  percent: number,
  aneuploidia: string,
  order: number,
  imagePath: string,
  riskOfMosaicism: number
}
