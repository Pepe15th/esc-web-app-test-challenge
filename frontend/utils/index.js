export const EMBRYO_FRAME_MINUTES = 30;

export function getEmbryoFrameOffset({ image_offset_minutes }) {
	return Math.round(image_offset_minutes / EMBRYO_FRAME_MINUTES);
}

export function formatFrameTime(frame) {
	const minutes = frame * EMBRYO_FRAME_MINUTES;
	const minutePart = minutes % 60;
	const hourPart = (minutes - minutePart) / 60;
	return `${hourPart}:${minutePart < 10 ? '0' : ''}${minutePart}`;
}
