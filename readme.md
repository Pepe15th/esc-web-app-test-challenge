# CATI web app


Requirements
------------

PHP 7.1.3 or higher with Composer installed.

Database server (MySQL preferred) with a new database + CATI annotator database (dump in Downloads section of BitBucket repository).

Node.js 6 or higher with Yarn installed.


Installation
------------

1. Copy `app/config/config.local.sample.neon` to `app/config/config.local.neon` and change it according to your environment.
	* Database `default` is the new database, database `cati` is the annotator database, with dumped data.

2. Make directories `temp/` and `log/` writable for the web server.

3. Install PHP dependencies using composer:

		composer install

4. Install frontend dependencies using yarn:

		yarn install

5. Optional: Create a `.env.local` file to configure frontend settings (see https://github.com/facebook/create-react-app/blob/v1.0.17/packages/react-scripts/template/README.md#advanced-configuration).

6. Either:

	a) run frontend dev server:

		yarn start

	b) build frontend assets:

		yarn build


Configuration
-------------

For configuration options, see https://doc.nette.org/cs/2.4/configuring.

Frontend was bootstrapped with Create React App, for more information, see
https://github.com/facebook/create-react-app/blob/v1.0.17/packages/react-scripts/template/README.md.


Scripts
-------

* List console commands:

		php www/index.php

* Generate a migration by comparing your current database to your mapping information:

		php www/index.php migrations:diff

* Execute migrations to the latest available version:

		php www/index.php migrations:migrate

* Check code style:

		composer check-cs

* Fix code style:

		composer fix-cs

* Run PHPStan:

		composer phpstan

* Run tests:

		composer test

* Run tests on Ubuntu:

		composer test -- -c tests/php-ubuntu.ini

* Run frontend dev server:

		yarn start

* Build frontend assets:

		yarn build

* Run JavaScript tests:

		yarn test


Challenge
---------

Implement a basic version of saving embryos to favorites.

### General instructions

* fork this repository
* clone the forked repository and install the project (see above), use `cati_annotator.sql` in repository Downloads to setup the annotator DB
* create a new branch
* implement the features (see below), commit as you work
* when you are done, push and create a pull request to the original repository

### PHP

* create a new entity for favorite embryo (with an association to User and ID of an embryo), create a Doctrine migration for creating a DB table
* create an POST API endpoint for adding an embryo to logged user's favorites (do nothing if the embryo is already in favorites)
* create a GET API endpoint for retrieving embryos in format required by React
	* see `embryos_sample.json` in repository Downloads for an example output
	* use `Netvor\Embryo\ApiModule\Model\EmbryoService` methods `getEmbryosByIds` and `formatEmbryo` to fetch and format the embryo data

### React

* use local component state if you want, you don't have to use Redux and/or sagas for the challenge
* add an "Add to favorites" button to `EmbryoDetails` component (located in `frontend/scenes/EmbryoLibrary/components/EmbryosGrid/components/Embryo/components/EmbryoDetails`)
	* clicking on the button will send POST request to your API endpoint to add the embryo to favorites
	* the button does not have to change when the embryo is already in favorites
* create a page for displaying favorite embryos
	* create a new component which will display several `EmbryoDetail` components (located in `frontend/components/EmbryoDetail`), you can use `embryos_sample.json` in repository Downloads for sample data
	* add a route the component to MainRouter (`frontend/components/MainRouter`) and a link to the route to Header (`frontend/components/Header`)
* connect the list component to your GET API - fetch the data and use them instead of the sample data
